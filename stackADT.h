#ifndef STACKADT_H
#define STACKADT_H


/*
*
*   LINKED LIST STACK TYPE
*   DYNAMICALLY ALLOCATED MEMORY IS MORE EFFICIENT THAN ARRAYS
*
*/


template <class Type>
struct nodeType
{
    Type info;
    nodeType<Type> *link;
};

template <class Type>
class linkedStackType: public stackADT<Type>
{
private:

     //pointer to the stack linked list
    nodeType<Type> *stackTop;  

    //private function called by COPY CONSTRUCTOR
    void copyStack(const linkedStackType<Type>& otherStack);



public:
    //overload assignment operator
    const linkedStackType<Type>& operator=
                                (const linkedStackType<Type>&);

    //function determines if stack is empty
    bool isEmptyStack() const;

    //if stack is full
    //THIS FUNCTION IS NOW IRRELEVANT!!!
        //--WHY? linked lists DON'T have a max stacksize.
        //this is only here because the ADT base class defines it
    bool isFullStack() const;

    //initialize stack to empty state.
    //POST: stackTop = NULL && elements are removed if any
    void intitializeStack();

    //POST: newItem to the top of the stack
    //PRE: stack exists and is NOT full
    void push(const Type& newItem);

    //function to return the top element of the stack.
    //PRE: stack exists and is not empty
    //POST: if stack is empty. terminate program
    Type top() const;

    //removes top element in stack
    void pop();





    //default constructor
    //POST: stackTop == NULL;
    linkedStackType();

    //COPY CONSTRUCTOR
    linkedStackType(const linkedStackType<Type>& otherStack);

    //destructor
    //POST: all elements are removed from stack
    ~linkedStackType();



};

#endif // STACKADT_H
