#ifndef H_binTreeNotes
#define H_binTreeNotes
#include <iostream>
#include <fstream>
#include <string>



using namespace std;



/*
**
*	ADT ADT ADT ADT ADT!!!!!!!!!!
*	CHAPTER 11 HEADER NOTES 
*	for binary tree ADT FUNCTIONS
*
**
*/

template <class type>
struct binNode {
	type info;
	BinNode *llink;
	BinNode *rlink;
};




template <class type>
class binaryTreeType {

public:

	//constructor
	binaryTreeType();
	~binaryTreeType();
	
	//COPY CONSTRUCTOR
	binaryTreeType(const binaryTreeType<type>& otherTree);
	
	//overloaded operator
	const binaryTreeType<type>& operator=
				(const binaryTreeType<type>&);

	bool isEmpty();	//check for empty tree
	
	//traversal calls
	void inOrderTraversal() const;
	void preOrderTraversal() const;
	void postOrderTraversal() const;

	//returns height, nodecount, leavescount
	int treeHeight() const;
	int treeNodeCount() const;
	int treeLeavesCount() const;
	
	//deallocation of binary tree (set root == NULL)
	void destroyTree();


protected:

	binNode *root;	//pointer to root node! (constructor sets to NULl)


private:	
	//shallow-copy pointer!! (consequently copies list)
	//link systemlinks for linux :D
	void copyTree(binNode<type>* &rootCopied, binNode<type>* OtherRoot)
	
	//calls destroyTree
	void destroy(binNode<type>* &p);
	
	// RECURSIVE traversals
	void inOrder(binNode<type> *p) const;
	void preOrder(binNode<type> *p) const;
	void postOrder(binNode<type> *p) const;
	
	//get height of tree
	//max needed for recursive height call
	int height(binNode<type> *p) const;
	int max(int x, int y) const;
	
	//returns # of nodes in tree (with or without children)
	int nodeCount(binNode<type> *p) const;
	//returns # of leaves (nodes without children)
	int leavesCount(binNode<type> *p) const;


};



/*
*
*
 *
 *
 *		IMPLEMENTATION
 *
 *
 *
*
*
*/

//constructor && destructor
template <class type>
binaryTreeType<type>::binaryTreeType() {
	root = NULL;
}

//isempty checker
template <class type>
bool binaryTreeType<type>::isEmpty() {
	return (root == NULL);
}

template <class type>
binaryTreeType<type>::~binaryTreeType() {

	//just call deletelist
	destroyTree(root);
}


//OVERLOADED ASSIGNMENT OPERATOR
template <class type>
const binaryTreeType<type>& binaryTreeType<type>::operator=
						(const binaryTreeType<type>& otherTree) {
							
			//avoid self-copy
		if(this != &otherTree) {	
				//if (this) has data, delete it.
			if (root != NULL) {
				destroy(root);	//recursive call (remember)
			}
			
				//if other binary tree is empty
			if (otherTree.root == NULL) 
				root = NULL;
			else
				copyTree(root, otherTree.root);
		}
		
		return *this;
}

//COPY CONSTRUCTOR
//calls copyTree function (base)
template <class type>
binaryTreeType<type>::binaryTreeType(const binaryTreeType<type>& otherTree) {
		
		if (otherTree.root == NULL) 	//if other tree is empty
			root = NULL;				//then this is too
			
		else	
			copyTree(root, otherTree.root);	
				
}

//SHALLOW COPYTREE FROM ROOT OTHER TO ROOTCOPIED(this)
template <class type>
void binaryTreeType::copyTree(binNode<type>* &rootCopied,
							  binNode<type>* OtherRoot) {
	if (rootOther == NULL)
		rootCopied = NULL;	//if tree is empty, well empty the other.

	else {
		//set dynamic pointer
		//set pointer info same as otherTreeRoot
		rootCopied = new binNode<type>;
		rootCopied->info = OtherRoot->info;
		//RECURSIVE SHALLOWCOPY CALL
		copyTree(rootCopied->llink, OtherRoot->llink);
		copyTree(rootCopied->rlink, OtherRoot->rlink);
	}
}


/*
	BASIC OPERATIONS
*/

//public call for treeLeavesCount
template <class type>
int binaryTreeType<type>::treeLeavesCount() const {
	return leavesCount(root);
}

//public call for nodeCount
template <class type>
int binaryTreeType<type>::treeNodeCount() const {
	return nodeCount(root);
}

template <class type>
int binaryTreeType<type>::nodeCount(binNode<type> *p) const {
	
    cout << "Write the definition of the function nodeCount"
         << endl;

    return 0;
}

template <class type>
int binaryTreeType<type>::leavesCount(binNode<type> *p) const {
	
    cout << "Write the definition of the function leavesCount"
         << endl;

    return 0;
}


//public treeHeight calls private: height()
template <class type>
int binaryTreeType::treeHeight() const {
	return height(root);
}


//private height called by treeHeight
template <class type>
int binaryTreeType::height(binNode<type> *p) const {

	if (p == NULL)	//or just check with isEmpty()
		return 0;	
	else			//else recursively check for maximum of 2 heights
		height(p) = 1 + max(height(p->llink), height(p->rlink));

		//where max determines the larger of the 2 integers (longest path)

}

//find larger of the nodes (left&right child nodes)
template <class type>
int binaryTreeType::max(int x, int y) const {
	if (x >= y)
		return x;
	else
		return y;
}


//destroy()called; by destroyTree which passes root as argument
//destroy() = private:: destroyTree() = public
template <class type>
void binaryTreeType<type>::destroy(binNode<type>* &p) {
	
	if (p != NULL) {
		destroy(p->llink);		//destroy left-subtree
		destroy(p->rlink);		//destroy right-subtree;
		
		delete p;
		p = NULL;
	}
}

//USE destroy() AND PASS ROOT NODE. as destroy(root);
template <class type>
void binaryTreeType<type>::destroyTree() {
	
	destroy(root);	//start from the root 
					//once called, destroy deletes left & right subtrees and deallocates memory
}

//////////////// 
/////TRAVERSAL TECHNIQUES////////
///////////////

/*
	inOrder traversal
		1)traverse left subtree
		2)visit node
		3)traverse right subtree
*/

//public traversal calls
template <class type>
void binaryTreeType<type>::inOrderTraversal() const {
	inOrder(root);
}

template <class type>
void binaryTreeType<type>::preOrderTraversal() const {
	preOrder(root);
}

template <class type>
void binaryTreeType<type>::postOrderTraversal() const {
	postOrder(root);
}

//PRIVATE CALLS
//PS ROOT MUST BE PASSED AS PARAMETER  *inOrder(root);
template <class type>
void binaryTreeType<type>::inOrder(binNode<type> *p) const {

	if (p != NULL) {
		inOrder(p->llink);	//step 1: traverse left-sub
		cout << p->info; << " ";	//step2": output
		inOrder(p->rlink);	//step 3: traverse right-sub
	}
}

/*
	Preorder Traversal
		1)Visit the node
		2)traverse left sub
		3)traverse right sub
*/
tepmlate <class type>
void binaryTreeType<type>::preOrder(binNode<type> *p) const {
	if (p != NULL) {
		cout << p->info << " ";		//step 1. output
		preOrder(p->llink);			//step 2
		preOrder(p->rlink);			//step 3
	}
}


/*
	Postorder traversal
		1)traverse left sub
		2)traverse right sub
		3)visit node
*/
template <class type>
void binaryTreeType<type>::postOrder(binNode<type> *p) const{
	if (p != NULL)
	{
		postOrder(p->llink);
		postOrder(p->rlink);
		cout << p->info << " ";
	}
}


#endif