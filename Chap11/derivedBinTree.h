#ifndef H_derivedBinTree
#define H_derivedBinTree

#include <iostream>
#include <string>
#include <fstream>

#include "binTreeNodes.h"
using namespace std;


/*
	DERIVED FROM binTreeNodes.h
	expanded from normal operations to include insert, delete, and searches
*/



template <class type>
class searchTreeType: public binaryTreeType<type>
{
public:
	//public call of searchItem() 
	bool search(const type& searchItem) const;
	//public call for insertItem()
	void insert(const type& insertItem);
	//public call for deleteItem()
	void deleteNode(const type& deleteItem);
	
private:

		//deletes node to to which p points
	void deleteFromTree(binNode<type>* &p);
	
};



/*
****IMPLEMENTATION!!!!!!!!
!!!!!!!!!!!!
!!!!!!!!!!!
:)
*/


//FOR SEARCH
template <class type>
bool searchTreeType<type>::search(const type& searchItem) const {
	
	bool found = false;
	//first round set current = root
	//then move current down to next node accordingly
	binNode<type> *current;
	
	
	//first attempt write from psuedo-code
	if (root == NULL) 
		cerr << "tree list empty" << endl;//tree empty
		
	else {
		current = root;
		
		while (current != NULL && !found) {
			
			if (current->info == searchItem)
				found = true;
			else if (current->info > searchItem) {	//if node is > searchItem
				current = current->llink;			//search left subtree
			}
			else
				current = current->rlink;			//search right subtree
		}
	}
}

//INSERT
//where duplicates are not allowed
//step 1. create new node & copy insertItem into node.
//step 1a. set rlink & llink to null
template <class type>
void searchTreeType<type>::insert(const type& insertItem) {
	
	//traversal pointers
	binNode<type> *current;
	binNode<type> *trailCurrent;	//points to the parent of current
	
	//step 1a. create new node
	binNode<type> *newNode = new binNode<type>;
	newNode->info = insertItem;
	newNode->llink = NULL;
	newNode->rlink = NULL;
	
	//step 1b
	if (root == NULL)	//empty binary tree. root points to newNode
		root = newNode;
	
	else {
		current = root;
		while (current != NULL)	{	//tree search alg for proper place
			trailCurrent = current;
			if (current->info == insertItem) {
				cerr << "NO DUPLICATES ALLOWED" << endl;
				return;	//exit
			}
			else if (current->info > insertItem) //then insertItem goes to left-subtree (smaller #'s)
				current = current->llink;
			
			else
				current = current->rlink;
		}//end search
		
		//insert node (trailcurrent should point at the last parent)
		if (trailCurrent->info > insertItem)			//insertItem becomes left child of trailCurrent
			trailCurrent->llink = newNode;			//insert newNode @ leftChild
		else											//insertItem becomes right child
			trailCurrent->rlink = newNode;			//insert newNode @rightChild
	}		
}


//DELETE
//deleteNode will call (private)::deleteFromTree(&*p)
//anytime a node is deleted, we adjust the parent tree

/*
CASES
	1:>> node to be deleted has no left & right subtrees (LEAF)
			-set parent node's link to NULL. & deallocate memory
	2:>> node to be deleted has no left Subtree
			-replace the parent node link node with the right child node & deallocate
	3:>> node to be deleted has no right subtree
			-replace the parent node link with the left link node & deallocate memory
	4:>> node to be deleted has both left & right subtrees
			-
*/

//STEPS//
//1. find node containing deleteItem
//2. delete the node. use deleteFromTree(trailCurrent->rlink)



//delete from tree
template <class type>
void searchTreeType<type>::deleteFromTree(binNode<type>* &p) {
	
	//traversal pointers
	binNode<type> *current;
	binNode<type> *trailCurrent;
	
	binNode<type> *temp;
	
	if (p == NULL)		//errchk
		cerr << "ERROR: node is NULL." << endl;
		
	else if (p->llink == NULL && p->rlink == NULL)	{		//if node pointer is a leaf (case 1)
		temp = p;			
		p = NULL;		//deallocate and call it quits
		delete temp;
	}
	else if (p->llink == NULL) {							//if p only contains right subtree (case 2)
		temp = p;
		p = temp->llink;
		delete temp;
	}
	else if (p->rlink == NULL) {
		temp = p;
		p = temp->rlink;
		delete temp;
	}
	else {
		current = p->llink;
		trailcurrent = NULL;
		
		while (current->rlink != NULL) {
			trailCurrent = current;
			current = current->rlink;
		}
		
		p->info = current->info;
		
		if (trailCurrent == NULL)		//current did not move
			p->llink = current->llink;	//current == p->llink; adjust p;
		else
			trailCurrent->rlink = current->llink;
		
		delete current;
	}
}


//searches for node, if found, passes that node to deleteFromTree(node to be deleted)
template <class type>
void searchTreeType<type>::deleteNode(const type& deleteItem) {
	
	bool found = false;
	
	//traversal nodes
	binNode<type> *current;
	binNode<type> *trailCurrent;
	
	if (root == NULL)
		cout << "ERR: EMPTY TREE" << endl;
		
	else {
		current = root;			//start from root
		trailCurrent = root;	//points to parent of current afterwards
		
		while (current != NULL && !found) {		//SEARCH FOR DELETEITEM
			if (current->info == deleteItem)
				found = true;
			else {
				
				//re-adjust search node and it's parent (current & trailCurrent)
				trailCurrent = current;
				
				if (current->info > deleteItem)		//move to left-subtree of current
					current = current->llink;
				else
					current = current->rlink;		//else move to right-subtree of current
					
				/*TRAILCURRENT = PARENT OF CURRENT*/
			}
		}//end search
		
		//after search 
		if (current == NULL)
			cout << "THE ITEM COULD NOT BE FOUND" << endl;
		else if (found) {
			if (current == root)
				deleteFromTree(root);
			else if (trailCurrent->info > deleteItem)
				deleteFromTree(trailCurrent->llink);
			else 
				deleteFromTree(trailCurrent->rlink);
		}//end if(found)
	}
}

#endif