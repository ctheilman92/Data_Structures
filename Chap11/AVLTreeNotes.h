#ifndef H_binTreeNotes
#define H_binTreeNotes
#include <iostream>
#include <fstream>
#include <string>


#include "binTreeNotes.h"
using namespace std;

/*

	AVL TREE (height-balanced tree)
		::heights of left & right subtrees differ by at most 1.
			---if x = node;	then |leftSubtreeOf(x) - rightSubtreeOf(x) | <= 1
		::left & right subtrees are AVL trees
		
		//THIS DEFINITELY PERTAINS TO DISCRETE MATH
		::total-nodes = 2(power[height]) - 1;
		
		BALANCE FACTOR(X) or bf(x) = xr - xl
		
		---where xl=left subtree && xr= right subtree---
			if (xl > xr) then node x is "LEFT HIGH" 
			if (xl = xr) then node x is "EQUAL HIGH"
			if (xl < xr) then node x is " RIGHT HIGH"
		
		

*/

template <class type>
struct AVLNode {
	
	type info;
	int bfactor;	//balance factor
	AVLNode<type> *llink;
	AVLNode<type> *rlink;
};



template <class type>
class AVLTreeType : public binaryTreeType<type> {
	
	
	
	
};

/*
	SEARCH ALGORITHM REMAINS THE SAME
*/



/*
IMPLEMENTATION
*/



/*
ROTATIONS
*/

template <class type>
void AVLTreeType<type>::rotateLeft(AVLNode<type>* &root) {
	
	AVLNode<type> *p;		//pointer to root of RIGHT SUBTREE
	
	//error checks
	if (root == NULL)
		cerr << "error in the tree" << endl;
	else if (root->rlink == NULL)
		cerr << "Error in the tree: no right subtree to rotate." << endl;
	
	else {
		p = root->rlink;			//set pointer to right subtree
		root->rlink = p->llink;		//left subtree of p becomes right subtree of root!
	
		p->llink = root;
		root = p;					//make p the new root
	}
}//end



//BALANCE FROM RECONSTRUCTS AT SPECIFIED NODE
//call the rotateLeft() & rotateRight() function
//called when subtree is LEFT DOUBLE HIGH and certain nodes need to be moved to right subtree
template <class type>
void AVLTreeType<type>::balanceFromLeft(AVLNode<type>* &root) {
	
	AVLNode<type> *p;
	AVLNode<type> *w;
	
	p = root->llink;		//p points to left subtree of root
	
	switch (p->bfactor)		//options dependent on BF(p)
	{
		case -1:
			root->bfactor = 0;
			p->bfactor = 0;
			rotateRight(root);
			break;
			
		case 0:
			cerr << "ERR: can't balance from the left." << endl;
			break;
		
		case 1:
			w = p->rlink;
			switch (w->bfactor) 	//adjust the BF'S
			{
				case -1:
					root->bfactor = 1;
					p->bfactor = 0;
					break;
				
				case 0:
					root->bfactor = 0;
					p->bfactor = 0;
					break;
				
				case 1:
					root->bfactor = 0;
					p->bfactor = -1;
			}
			w->bfactor = 0;
			rotateLeft(p);
			root->llink = p;
			rotateRight(root);
	}//kill-switch
}//


//rotate from left subtree to right 
template <class type>
void AVLTreeType<type>::rotateRight(AVLNode<type>* &root) {
	
	AVLNode<type> *p;
	
	//ERROR CHECKS
	if (root == NULL)
		cerr << "error in the tree" << endl;
	else if (root->llink == NULL)
		cerr << "Error in the tree: no left subtree to rotate." << endl;
		
	else {
		p = root->llink;
		root->llink = p->rlink;		//set right subtree of p becomes left subtree of root
		
		p->rlink = root;			//make p the new root
		root = p;
	}
	
}