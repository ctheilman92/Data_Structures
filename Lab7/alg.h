#ifndef H_ALG
#define H_ALG

#include <iostream>
using namespace std;






/******************
*******************

Cameron Heilman
class header for HT

	:modular division model: h(X) = x % HTSize (see pg. 512)
	
	:linear probe model: (h(x) + j) % HTSize; (see pg. 513)

*******************
******************/












template <class elemType>
class hashT 
{

public:

	int getTotalCols() const;
		//modular division function
		//determines HT position by (key % HTSize);
	int modDiv(elemType elemKey);

	void HTprint() const;

	void linearProbe(elemType elemKey);

	void postSearch(elemType keyChk);

	hashT(elemType array[], int size);

		//array & index list are deleted
	//~hashT();

private:

	elemType *HT;	//pointer to HT
	int *tempArray;	//pointer to array indicating
							//status of position
	//int length;
	int HTSize;
	int totalCols;
};


//constructor
template <class elemType>
hashT<elemType>::hashT(elemType array[], int size)
{	
	HTSize = size;
	HT = array;
	totalCols = 0;
}

/***************************
=======================
=======================
IMPLEMENTATION (PG. 522)
=======================
=======================
***************************/




//MODULAR DIVISION POPULATION METHOD (PG. 512)
//RETURN POSITION TO PLACE IN HTable
template <class elemType>
int hashT<elemType>::modDiv(elemType elemKey)
{	return elemKey % HTSize;	}



//LINEAR PROBING FOUND ON 514
//++++++edited+++++++
template <class elemType>
void hashT<elemType>::linearProbe(elemType elemKey)
{
	int hIndex = modDiv(elemKey);
	bool found = false;	


	while (HT[hIndex] != 0 && !found) {

		if (HT[hIndex] == elemKey)
		{
        	found = true;
		}
    	else
    	{
    		//sequential search for next available
        	hIndex = (hIndex + 1) % HTSize;
    	}
    }

	if (found)
		std::cerr << "Duplicate items are not allowed." << endl;
	else
		HT[hIndex] = elemKey;
}


template <class elemType>
void hashT<elemType>::HTprint() const
{
	cout << endl << "Method: Modular Division. " 
		<< endl << "Hashed List: " << endl
		<< "SUB" << "	KEY" << endl << endl
		<< "======================================" << endl;

	for (int i = 0; i < HTSize; i++)
	{
		cout << i << "	" << HT[i] << endl;
	}

	// cout <<  endl << "Total Collisions detected: "
	// 	<< colCount << endl;

}


template <class elemType>
void hashT<elemType>::postSearch(elemType keyChk)
{
	bool found = false;
	int colsPer = 0;
	int homeAdd;	//original index check (final resting place for any found variable in HT)
	int chkAdd;	//checking index to compare values

	//find value in HTable
	for (int i = 0; i < HTSize; i++) {
		
		if (HT[i] == keyChk) {
			found = true;
			homeAdd = i;
			chkAdd = modDiv(keyChk);
			while (chkAdd != homeAdd) {
				colsPer++;
				totalCols++;
				chkAdd = (chkAdd + 1) % HTSize;
			}
		}
	}
	if (!found)
		homeAdd = modDiv(keyChk);


	chkAdd = homeAdd;
	//check for collisions (algorithm for values not in HT)
	while (HT[chkAdd] != 0 && !found) {

		colsPer++;	//inc. single key collision counter
		totalCols++;	//inc. total key collisions counter
				
		//sets home address for next available slot
		chkAdd = (chkAdd + 1) % HTSize;
	}

	//format to make it all appear on one line
	cout << keyChk << "    ";
	if (found)
		cout << "YES          ";
	else
		cout << "NO           ";

	cout << homeAdd << "            ";
	cout << colsPer << endl;
	//ends everything
}

template <class elemType>
int hashT<elemType>::getTotalCols() const
{	return totalCols;	}



#endif