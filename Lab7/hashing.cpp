#include <iostream>
#include <fstream>
#include "alg.h"

using namespace std;


/*****************
*
*	CAMERON HEILMAN - 
*		COSC 2437-LAB 7 hash algorithms
*
*	Using MODULAR DIVISION for propogation of HT
*	Using Linear Probing for collision detection
*
*	*reference page 521 for hash algorithm
*
*		::	SEARCH VALUES FILE(lab7SRCH.dat) passed from argv[1]
*		::  HASH TABLE[13] populated by program proessed file (lab7.dat)
*
******************/


int main(int argc, char *argv[])
{

	int key;		//number stored
	int hti;		//index of HTable[]
	int chkVal;

		//CLASS HOLDS POINTERs TO ARRAYS
		//INITIALIZE 0 to represent empty slot
	int HTable[13] = { 0 };
	

	ifstream HTFILE;	//lab7.dat	(original HT populated with this)
	ifstream CHKFILE;	//lab7srch.dat (search values)
	string htstring = "lab7.dat";
	string chkstring = "lab7srch.dat";


	//object creation
	hashT<int> hashObj(HTable, 13);

	
	// if (argc < 2) {        
	// // Tell the user how to run the program
 //    std::cerr << "EXECUTE ERR!!!!!!!!!" << endl;
 //    std::cerr << "Usage1: " << argv[0] << " 'VALUE CHECKING FILE' " << endl;

 //        return 1;		
	// }
	


	HTFILE.open(htstring);
	if (HTFILE.is_open())
	{
		cout << "+++++++++++++++++++++++++++++++" << endl
			<< "calculating hashed position for keys..." << endl
			<< "++++++++++++++++++++++++++++++++" << endl << endl;
		while (HTFILE >> key)
		{
			//debug
			//hti is h(key) = key % HTSize (original position)
			hti = hashObj.modDiv(key);

			//collision handling
			hashObj.linearProbe(key);
			
		
		}

		//hashed array list (0's represent empty slots)
		hashObj.HTprint();
		//reads in from lab7srch.dat
		CHKFILE.open(chkstring);
		if (CHKFILE.is_open())
		{
			//header
			cout << endl << endl 
				<< "SEARCH RESULTS FROM lab7srch.dat: " << endl << endl
				<< "KEY    FOUND	HOME ADDRESS	COLLISIONS" << endl
				<< "==========================================" << endl;

			while (CHKFILE >> chkVal) {
				hashObj.postSearch(chkVal);
			}


			cout << "TOTAL NUMBER OF COLLISIONS: " << hashObj.getTotalCols() << endl << endl;
		
			CHKFILE.close();
		}
		else {
			std::cerr << "EXECUTION FAILED: lab7srch.dat could not be accessed!..." << endl << endl;
		}


		HTFILE.close();
	}
	else {	//this validates htfile lab7srch.dat
	
		std::cerr << "EXECUTION FAILED: lab7.dat could not be accessed!..." << endl << endl;
		return 1;
	}



	return 0;
}