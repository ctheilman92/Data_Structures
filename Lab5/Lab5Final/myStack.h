#ifndef STACKTYPE_H
#define STACKTYPE_H

#include <iostream>
#include <cassert>
#include <string>

#include "stackADT.h"
using namespace std;


/*
 *	CAMERON HEILMAN
 *	COSC-2437-003
 *
 *	**HEADER/IMPLEMENTATION FILE FOR MYSTACK CLASS -> DERIVED FROM STACKADT.H
 *
 * STACKTYPE CLASS DEFINITION
 *   UTILIZE STL: #include <stack>
 *   http://www.dreamincode.net/forums/topic/57497-stl-stack/
 *
 *
 */

 
template <class Type>
class stackType :public stackADT<Type>
{
private:
    int maxStackSize;   //holds max stack size
    int stackTop;       //points to the top element in stack
    Type *list;     //pointer to the array holding the elements

    //copy this stack to the other stack which was created
    void copyStack(const stackType<Type>& otherStack);



public:

    //overload the assignment operator
    //passes oother object of stackType as param
    const stackType<Type>& operator=(const stackType<Type>&);

    //derived function initialize empty stack
    void initializeStack();

    //determines if stack is empty
    bool isEmptyStack() const;

    //determines if stack is full
    bool isFullStack() const;

    //enters new item to the top of the stack
    void push(const Type& newItem);

    //returns top-stack element
    //IF stack is empty, program terminates.
    Type top() const;

    //removes top element from stack
    void pop();

    //CONSTRUCTOR
    //initialize stack size of 100 elements
    //POST: stackTop = 0, maxStackSize = stackSize
    stackType(int stackSize = 100);


    //COPY CONSTRUCTOR
    stackType(const stackType<Type>& otherStack);

    //destructor
    //REMOVE ALL ELEMENTS OF STACK- ARRAY IS DELETED
    ~stackType();


};


/*
 *  PLACE MEMBER FUNCTION DEFINITIONS HERE
 *  summary & time complexity: see 7-1. p. 409
 */



/*
 *
 *
 *
 *
 * THIS SECTION:
 *
 *      -CONSTRUCTOR
 *      -DESTRUCTOR
 *      -COPY CONSTRUCTOR
 *      -copyStack(otherStack) PRIVATE MEMBER FUNCTION
 *      -ASSIGNMENT OPERATOR OVERLOAD
 *
 *
 *
 */


//CONSTRUCTOR
//sets maxStackSize from user input
//if not specified by user, default size = 100
//sets stackTop to 0, & creates appropriate array[maxStackSize]
template <class Type>
stackType<Type>::stackType(int stackSize)
{
    if (stackSize <= 0)
    {
        cout << "size MUST be positive. " << endl;
        cout << "creating an array of size 100" << endl;

        maxStackSize = 100;
    }
    else
        maxStackSize = stackSize;   //set passed value to private maxStacksize

    stackTop = 0;                   //set stackTop to 0
    list = new Type[maxStackSize];  //create array

}

//DESTRUCTOR
//deallocates memory of array(the stack)
//set stackTop to 0
template <class Type>
stackType<Type>::~stackType()
{
    delete [] list;
}


//COPY CONSTRUCTOR
//when stackType object is created by:
//passing another stackType object as a parameter
//list is copied through copyStack(otherStack) function!
template <class Type>
stackType<Type>::stackType(const stackType<Type>& otherStack)
{
    //pointer to the stack obj that's making the call
    //make sure 2 stacks don't point to the same thing
    list = NULL;

    //call private function
    copyStack(otherStack);
}

//USE THIS TO IMPLEMENT COPY-CONSTRUCTOR
//OVERLOAD THE = OPERATOR
template <class Type>
void stackType<Type>::copyStack(const stackType<Type>& otherStack)
{
    //if a list does exist. delete it.
    delete [] list;

    maxStackSize = otherStack.maxStackSize;
    stackTop = otherStack.stackTop;
    list = new Type[maxStackSize];

    //copy elements
    for (int j = 0; j < stackTop; j++)
    {   list[j] = otherStack.list[j];   }

}

//OVERLOADED '=' OPERATOR
//returns the stackType OBJ using *this pointer
template <class Type>
const stackType<Type>& stackType<Type>::
    operator=(const stackType<Type>& otherStack)
{
    if (this != &otherStack) //avoid self-copy
        copyStack(otherStack);

    //returns calling obj (specifies operation)
    return *this;
}

/*
 *
 *
 *
 *
 * BASIC STACK OPERATIONS
 *
 *
 *
 *
 */

template <class Type>
void stackType<Type>::initializeStack()
{
    stackTop = 0;
}

template <class Type>
bool stackType<Type>::isEmptyStack() const
{
    //returns T, if stackTop is 0
    return(stackTop == 0);
}


template <class Type>
bool stackType<Type>::isFullStack() const
{
    //if we hit maxStackSize, return TRUE
    return(stackTop == maxStackSize);
}

template <class Type>
void stackType<Type>::push(const Type& newItem)
{
    //see 7-8. p. 404
    //1. store newItem in the array[p] indicated by stackTop
    //2. increment stackTop


    //overflow error check
    if (!isFullStack())
    {
            //ESSENTIAL OPERATIONS OF FUNCTION
        list[stackTop] = newItem;
        stackTop++;
    }

    else
        cout << "STACK IS FULL. SORRY!" << endl << endl;
}


template <class Type>
Type stackType<Type>::top() const
{
    //terminate program if stack is empty
    assert(stackTop != 0);

            //since stackTop = 1 starts list[0]
    return list[stackTop -1];

}

template <class Type>
void stackType<Type>::pop()
{
    //see 7-9. P.406
    //JUST DECREMENT stackTop

    //underflow check
    if (!isEmptyStack())
        stackTop--;
    else
        cout << "STACK IS EMPTY NOTHING TO POP" << endl << endl;

}

#endif // STACKTYPE_H
