#ifndef INFIXTOPOSTFIX_H
#define INFIXTOPOSTFIX_H

#include "myStack.h"
#include <iostream>
#include <string>
using namespace std;

/*
CAMERON HEILMAN
LAB 5::COSC-2437

**HEADER FILE FOR INFIXTOPOSTFIX CLASS**
*/



 //	holds pfx, infx
 //	and holds member functions for calculating precedence, converting, fetching, and printing
 

 template <class T>
 class infixToPostfix
 {
 private:

 	string pfx;
 	string infx;

 public:

 	//to constructor sets pfx to empty string
 	infixToPostfix();

    ~infixToPostfix();


 	//stores infix expression
 	//read more into function definitions later
 	void getInfix(string in);

 	//output infix expression
    string showInfix();

 	//output postfix expression
    string showPostfix();

 	//conversion to postFix and stores in pfx
 	template <class C>
 	void convertToPostfix(stackType<C>&);

 	//determines precedence between 2 operators
 	//if (stack.top() >= sym), return true. else return false;
 	template <class C>
    bool precedence(char, stackType<C>&);

 };



#endif

