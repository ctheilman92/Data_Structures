Lab5 README
/////////////////////////

Task: program that converts infix expressiosn to postfix expressions
	-input from file: "testData.txt"
	-output to file: "cheilmanLab5.out"
	-USE LINUX COMMANDS!!: "$ ./Lab5 > cheilmanLab5.out"
		-could also read input from a file: "$while read i ; do ./lab5 $i ; done < testData.txt"

	-Combined would be this:
		"$ while read i ; do ./lab5 $i ; done < testData.txt > cheilmanLab5.out"......right?
++++++++++++++
++++++++++++++
Formats:
	infix: A+B-C;
	postfix: AB+C-;
++++++++++++++
++++++++++++++
classes:
	1)stackADT
	2)myStack
	3)infixToPostfix

++++++++++++++
++++++++++++++
infixToPostfix Member-functions:

::<void>	getInfix-stores infix expression;
::<string>	showInfix-Outputs infix expression;
::<string>	showPostfix-Outputs the postfix expression
::<void>	convertToPostfix-converts infix exp. to postfix and stored in pfx
::<bool>	precedence-determines precedence between 2 operators.
			>>if (op1 > op2), then return true; else return false
++++++++++++++
++++++++++++++
Test-Values:
	
::	A + B - C;
::	(A + B) * C;
::	(A + B) * (C - D);
::	A + ((B + C) * (E - F)
::	G + H;
++++++++++++++
++++++++++++++
FILES FOR SUBMISSION

	1) stackADT.h
	2) myStack.h
	3) infixToPostfix.h
	4) infixToPostfix.cpp
	5) cheilmanLab5.out
	6) testData.txt
	7) makefile
++++++++++++++
++++++++++++++	
