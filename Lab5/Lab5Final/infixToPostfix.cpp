#include <iostream>
#include <string>

#include "infixToPostfix.h"
#include "myStack.h"

using namespace std;


/*
CAMERON HEILMAN
LAB 5::COSC-2437
**IMPLEMENTATION FILE FOR INFIXTOPOSTFIX CLASS**
*/


//constructor
template <class T>
infixToPostfix<T>::infixToPostfix()
{}


//define destructor later
//deallocate memory
template <class T>
infixToPostfix<T>::~infixToPostfix()
{	
	//set to 
	infx.clear();
	pfx.clear();
}


template <class T>
void infixToPostfix<T>::getInfix(string expr)
{	infx = expr;	}

template <class T>
string infixToPostfix<T>::showInfix()
{	return infx;	}

template <class T>
string infixToPostfix<T>::showPostfix()
{	return pfx;		}


//pass stack in
//traverse the infx string and operate
template <class T>
template <class C>
void infixToPostfix<T>::convertToPostfix(stackType<C>& stackObj)
{
	//step a. init pfx to empty expression, and initialize empty stack
    pfx.clear();
    stackObj.initializeStack();

	bool nextp = false;

	for (int it = 0; it < infx.length(); it++)
	{
		//manipulate 
		char sym = infx[it];
		//STEP B.4
        if (sym == '+' || sym == '-' || sym == '*' || sym == '/')
		{
			nextp = false;
			cout << "LOG:> operator:: " << sym << endl;
			//b.4.1 pop & append all operators from stack to pfx
			//that are above the most recent parenthesis and have 
			//precedence greater than or equal to sym
			if (!stackObj.isEmptyStack())
			{
                //if (stackObj.top() != '(')
				//{
					cout << "call precedence: for sym: " << sym << " & stack.top: " << stackObj.top() << endl << endl;
					
					//if stack obj is greater >= sym 
                    if (precedence(sym, stackObj))
					{
						//doublecheck nextp flag and check the precedence of the next stackObj.top()
						while (stackObj.top() != '(' && !nextp && precedence(sym, stackObj))
						{
							cout << "LOG:> stackObj " << stackObj.top() << " >= sym " << sym << endl;
							cout << "LOG:> append " << stackObj.top() << " to pfx" << endl;
							cout << "LOG:> pop " << stackObj.top() << " from stack" << endl;
							//pop and append all operators >= sym
                        	pfx += stackObj.top();
							stackObj.pop();

							if (stackObj.isEmptyStack())
							{
								cout << "LOG:> STRING EMPTY AFTER POP!" << endl;
								cout << "LOG:> push " << sym << " to stack" << endl;
								stackObj.push(sym);
								nextp = true;
							}
						}
					}

				//}

			}
			if (!nextp)
			{
				cout << "LOG:> push " << sym << " to stack." << endl << endl;
				stackObj.push(sym);
				//nextp = true;
			}
		}
		//STEP B.2
        else if (sym == '(')
        {
        	cout << "LOG:> left paren:: " << sym << endl
        		<< "LOG:> push to stack. " << endl << endl;
			stackObj.push(sym);
		}
		//STEP B.3
        else if (sym == ')')
		{
			cout << "LOG:> right paren:: " << sym << endl;
			if (!stackObj.isEmptyStack())
			{
				while (stackObj.top() != '(')
				{
					//pop & append all chars until '('
					cout << "LOG:> " << stackObj.top() << " app. to pfx." << endl;
					pfx += stackObj.top();
					stackObj.pop();
				}


                if (stackObj.top() == '(')
				{
					//discard left paren//
					cout << "LOG:> " << stackObj.top() << " discarded." << endl << endl;
					stackObj.pop();
				}
			}
		}
		//STEP B.1
		else
		{   
			cout << "LOG:> operand:: " << sym << endl;          //if operand, append to pfx
			cout << "LOG:> app. to pfx" << endl << endl;
			pfx += sym;
		}
	}


	//STEP C.
	//after processing infx. 
	//append rest of operators to pfx
	cout << "LOG:> pop & append remaining stack items to pfx" << endl;
    while (!stackObj.isEmptyStack())
	{
		pfx += stackObj.top();
		stackObj.pop();
	}
}


template <class T>
template <class C>
bool infixToPostfix<T>::precedence(char sym, stackType<C>& stackObj)
{
	int symChk;
	int stackChk;
	

    if (stackObj.top() == '+' || stackObj.top() == '-' || stackObj.top() == '*' || stackObj.top() == '/')
	{
		//easy comparison of weight
		if (sym == '*' || sym == '/')
			symChk = 1;
		else
			symChk = 0;

		if (stackObj.top() == '*' || stackObj.top() == '/')
			stackChk = 1;
		else 
			stackChk = 0;


		//if stackChk >= symChk ::return true::
		//else ::return false::
		if (stackChk >= symChk)
			return true;
		else
			return false;

	}
	//else if(stackObj.top() == '(')
	//	return false;
}




