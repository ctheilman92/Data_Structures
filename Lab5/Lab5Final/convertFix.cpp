#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>


#include "myStack.h"
#include "infixToPostfix.h"
#include "infixToPostfix.cpp"

using namespace std;


/*
CAMERON HEILMAN
LAB 5::COSC-2437

**MAIN DRIVER FILE**

this driver will read data from file/single argument expression and write the program output to file from terminal execute 
file-IN: testData.txt
file-OUT: cheilmanLab5.out

EXAMPLE OF FULL INPUT AND OUTPUT
	"$./convertFix testData.txt" > cheilmanLab5.out"
	
	***BONUS FUNCTIONALITY***
		.accept argv as either a single expression or read content from a file on program execute
		.example of single expression test::
			"$./convertFix a+b*c > cheilmanLab5.out"
*/

int main(int argc, char *argv[])
{
	string expr;
	ifstream expFile;

	//dynamically create fixObj. manual stackObj;
	infixToPostfix<string> * fixObj;
	stackType<char> stackObj(20);


	if (argc < 2) {
        // Tell the user how to run the program
        std::cerr << "EXECUTE ERR!!!!!!!!!" << endl;
        std::cerr << "Usage1: " << argv[0] << " 'FILENAME' " << endl << "OR" << endl;
        std::cerr << "Usage2: " << argv[0] << " A+B*C (infix expression) " << endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }
	

	expFile.open(argv[1]);
	if (expFile.is_open())
	{
		cout << endl << "reading file contents:: " << endl;
		while (expFile >> expr)
		{

			//dynamically create object for infix & postfix
			//read in infix expression

			fixObj = new infixToPostfix<string>;
			fixObj->getInfix(expr);

			//conversion process
			cout << "Process INFIX:	" << fixObj->showInfix() << endl << endl;
			fixObj->convertToPostfix(stackObj);

		cout << endl << "processed!" << endl;
		cout << "++++++++++++++++++++++++++++++++" << endl
			<< "POSTFIX:	" << fixObj->showPostfix() << endl
			<< "+++++++++++++++++++++++++++++++++" << endl << endl;

			//debug
			/*
			cout << "Infix:  " << fixObj->showInfix() << endl;
			cout << "Postfix: " << fixObj->showPostfix() << endl;
			*/
			//delete object for this expression
			delete fixObj;
		}
		expFile.close();
	}
	else
	{
		cout << endl << "Argument is Not a file..." << endl;
		fixObj = new infixToPostfix<string>;
		fixObj->getInfix(argv[1]);



		//conversion process
		cout << "Process INFIX:	" << fixObj->showInfix() << endl << endl;
		fixObj->convertToPostfix(stackObj);

		cout << endl << "processed!" << endl;
		cout << "++++++++++++++++++++++++++++++++" << endl
			<< "POSTFIX:	" << fixObj->showPostfix() << endl
			<< "+++++++++++++++++++++++++++++++++" << endl << endl;

		delete fixObj;
	}

	return 0;
}