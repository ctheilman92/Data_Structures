#ifndef H_AVLTree
#define H_AVLTree
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

template <class Type>
class Node
{
public:
    Type info;
    int	bfactor; // balance factor
    Node* left;
    Node* right;
};

template <class Type>
class AVLTree
{
public:
    	void insert(const Type &item);
    	void initializeTree();
    	void rotateLeft(Node<Type>* &root);
    	void rotateRight(Node<Type>* &root);
    	void balanceLeft(Node<Type>* &root);
    	void balanceRight(Node<Type>* &root);
    	void insertIntoAVL(Node<Type>* &root, Node<Type>  *newNode, bool& isTaller);
    	string readFile();	//return collected string to main file
    	void writeFile();
	void showCounter();
	void outputInOrder(Node<Type>* &root);
	void outputPostOrder(Node<Type>* &root);
	AVLTree();   //default constructor
	~AVLTree();


    	Node<Type>* root;
private:
    	int counter;        //total nodes in tree
    	ofstream outputFile;    //arodriguez108lab9.out
};

template<class Type>
AVLTree<Type>::~AVLTree()
{
	cout << "Closing output file: arodriguez108lab9.out" << endl;
    	outputFile.close();
}

template<class Type>
void AVLTree<Type>::initializeTree()
{
    
	cout << "Creating output file: cheilmanlab9.out" << endl;
	outputFile.open("cheilmanlab9.out");
    
}

template <class Type>
void AVLTree<Type>::rotateLeft(Node<Type>* &root)
{
    Node<Type> *p;  //pointer to the root of the
                        //right subtree of root
    if (root == NULL)
        cerr << "Error in the tree" << endl;
    else if (root->right == NULL)
        cerr << "Error in the tree:"
             <<" No right subtree to rotate." << endl;
    else
    {
        p = root->right;
        root->right = p->left; //the left subtree of p becomes
                                //the right subtree of root
        p->left = root;
        root = p;	//make p the new root node
    }
}//rotateLeft

template <class Type>
void AVLTree<Type>::rotateRight(Node<Type>* &root)
{
    Node<Type> *p;  //pointer to the root of
                        //the left subtree of root

    if (root == NULL)
        cerr << "Error in the tree" << endl;
    else if (root->left == NULL)
        cerr << "Error in the tree:"
             << " No left subtree to rotate." << endl;
    else
    {
        p = root->left;
        root->left = p->right; //the right subtree of p becomes
                                //the left subtree of root
        p->right = root;
        root = p;	//make p the new root node
    }
}//end rotateRight

template <class Type>
void AVLTree<Type>::balanceLeft(Node<Type>* &root)
{
    Node<Type> *p;
    Node<Type> *w;

    p = root->left;   //p points to the left subtree of root

    switch (p->bfactor)
    {
    case -1:
        root->bfactor = 0;
        p->bfactor = 0;
        rotateRight(root);
        break;

    case 0:
        cerr << "Error: Cannot balance from the left." << endl;
        break;

    case 1:
        w = p->right;
        switch (w->bfactor)  //adjust the balance factors
        {
        case -1:
            root->bfactor = 1;
            p->bfactor = 0;
            break;

        case 0:
            root->bfactor = 0;
            p->bfactor = 0;
            break;

        case 1:
            root->bfactor = 0;
            p->bfactor = -1;
        }//end switch

        w->bfactor = 0;
        rotateLeft(p);
        root->left = p;
        rotateRight(root);
    }//end switch;
}//end balanceFromLeft

template <class Type>
void AVLTree<Type>::balanceRight(Node<Type>* &root)
{
    Node<Type> *p;
    Node<Type> *w;

    p = root->right;   //p points to the left subtree of root

    switch (p->bfactor)
    {
    case -1:
        w = p->left;
        switch (w->bfactor)  //adjust the balance factors
        {
        case -1:
            root->bfactor = 0;
            p->bfactor = 1;
            break;

        case 0:
            root->bfactor = 0;
            p->bfactor = 0;
            break;

        case 1:
            root->bfactor = -1;
            p->bfactor = 0;
        }//end switch

        w->bfactor = 0;
        rotateRight(p);
        root->right = p;
        rotateLeft(root);
        break;

    case 0:
        cerr << "Error: Cannot balance from the left." << endl;
        break;

    case 1:
        root->bfactor = 0;
        p->bfactor = 0;
        rotateLeft(root);
        
    }//end switch;
}//end balanceFromRight

template <class Type>
void AVLTree<Type>::insertIntoAVL(Node<Type>* &root, Node<Type>  *newNode, bool& isTaller)
{
    if (root == NULL)
    {
        root = newNode;
        isTaller = true;
    }
    else if (root->info == newNode->info)
        cerr << "No duplicates are allowed." << endl;
    else if (root->info > newNode->info) //newItem goes in
                                         //the left subtree
    {
        insertIntoAVL(root->left, newNode, isTaller);

        if (isTaller) //after insertion, the subtree grew in height
            switch (root->bfactor)
            {
            case -1:
                balanceLeft(root);
                isTaller = false;
                break;

            case 0:
                root->bfactor = -1;
                isTaller = true;
                break;

            case 1:
                root->bfactor = 0;
                isTaller = false;
            }//end switch
        }//end if
        else
        {
            insertIntoAVL(root->right, newNode, isTaller);

            if (isTaller) //after insertion, the subtree grew in
                          //height
                switch (root->bfactor)
                {
                case -1:
                    root->bfactor = 0;
                    isTaller = false;
                    break;

                case 0:
                    root->bfactor = 1;
                    isTaller = true;
                    break;

                case 1:
                    balanceRight(root);
                    isTaller = false;
                }//end switch
        }//end else
}//insertIntoAVL


template<class Type>
void AVLTree<Type>::insert(const Type &item)
{
    bool isTaller = false;
    Node<Type>  *newNode;

    newNode = new Node<Type>;
    newNode->info = item;
    newNode->bfactor = 0;
    newNode->left = NULL;
    newNode->right = NULL;

    insertIntoAVL(root, newNode, isTaller);
}

template<class Type>
AVLTree<Type>::AVLTree()
{
    root = NULL;
    counter = 0;
}

template<class Type>
void AVLTree<Type>::showCounter()
{
	cout << "Total Nodes: " << counter << endl;
	outputFile << "Total Nodes: " << counter << endl;
}

template<class Type>
string AVLTree<Type>::readFile()
{
	cout << "Reading data from: lab9.dat" << endl;
	outputFile << "Reading data from: lab9.dat" << endl;

    ifstream inputFile("lab9.dat");		//open file: lab9.dat
	Type sym;				//hold char input
    string line;				//hold line of input

	cout << "PreOrder: ";
	outputFile << "PreOrder: ";

    	int n = 0;			//index
	while(inputFile >> sym)			
	{
		line[n] = sym;
		insert(line[n]);	//insert info into AVLTree
		cout << line[n];
		outputFile << line[n];
		n++;
		counter++;		//count total nodes in tree
	}

	cout << endl;
    	outputFile << endl;
	
	inputFile.close();	

	return line;	
}

template<class Type>
void AVLTree<Type>::writeFile()
{
	//InOrder
	cout << "InOrder: ";
	outputFile << "InOrder: ";
	outputInOrder(root);
	outputFile << endl;
	cout << endl;

	//PostOrder
	cout << "PostOrder: ";
	outputFile << "PostOrder: ";
	outputPostOrder(root);
	outputFile << endl;
	cout << endl;

	cout << "Finished..." << endl;
}



//NEED THIS FOR LAB
template<class Type>
void AVLTree<Type>::outputInOrder(Node<Type>* &root)
{
	if(root->left != NULL)
		outputInOrder(root->left);	//advance to left node
	

	cout << root->info;
	outputFile << root->info;

	if(root->right != NULL)			
	{		
		if(root->right->left != NULL)				//if the right node exists, check if it has a left leaf
		{	outputInOrder(root->right->left);	}	//if true, move to that leaf
		
		cout << root->right->info;				
		outputFile << root->right->info;

		if(root->right->right != NULL)				//if the right node exists, check if it has a right leaf
		{	outputInOrder(root->right->right);	}	//if true, move to that leaf
	}	
}


//NEED THIS FOR LAB
template<class Type>
void AVLTree<Type>::outputPostOrder(Node<Type>* &root)
{
	if(root->left != NULL)			
	{
		outputPostOrder(root->left);	//advance to node[0]
	}

	if(root->right != NULL)
	{
		outputPostOrder(root->right);	//advance to right node
	}

	outputFile << root->info;
	cout << root->info;
}

#endif
