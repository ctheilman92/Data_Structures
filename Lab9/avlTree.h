#ifndef H_AVLTree
#define H_AVLTree
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
using namespace std;


/*
CAMERON HEILMAN IMPLEMENTATION 
LAB 9 AVL TREES

    :FUNCTIONS STILL NEEDED:
        -inOrder
        -preOrder (just call the list)
        -postOrder
*/



template <class elemT>
class AVLNode
{
public:
    elemT info;
    int	bfactor; // balance factor
    AVLNode* llink;
    AVLNode* rlink;
};


template <class elemT>
class AVLTreeType
{
public:

    void InOrder(AVLNode<elemT>* &root);
    void rotateToLeft(AVLNode<elemT>* &root);
    void rotateToRight(AVLNode<elemT>* &root);
    void balanceFromLeft(AVLNode<elemT>* &root);
    void balanceFromRight(AVLNode<elemT>* &root);
    
    void insert(const elemT &newItem);  //this will call the below function
    void insertIntoAVL(AVLNode<elemT>* &root, AVLNode<elemT>  *newNode, bool& isTaller);
    
    //read from File
    void readFromFile(string);
    //write to file
    void writeMethods();
    //show nodecount (should be 14)
    void showCounter();
    //for lab9
	void outputPostOrder(AVLNode<elemT>* &root);
    
    //constructor
    AVLTreeType();   //default constructor
    ~AVLTreeType();

    AVLNode<elemT>* root;
    
private:

    int counter;
    ofstream outputFile;

};




/*

IMPLEMENTATION

*/


template <class elemT>
void AVLTreeType<elemT>::rotateToLeft(AVLNode<elemT>* &root)
{
    AVLNode<elemT> *p;  //pointer to the root of the
                        //right subtree of root
    if (root == NULL)
        cerr << "Error in the tree" << endl;
    else if (root->rlink == NULL)
        cerr << "Error in the tree:"
             <<" No right subtree to rotate." << endl;
    else
    {
        p = root->rlink;
        root->rlink = p->llink; //the left subtree of p becomes
                                //the right subtree of root
        p->llink = root; 
        root = p;	//make p the new root node
    }
}//rotateLeft

template <class elemT>
void AVLTreeType<elemT>::rotateToRight(AVLNode<elemT>* &root)
{
    AVLNode<elemT> *p;  //pointer to the root of	
                        //the left subtree of root

    if (root == NULL)
        cerr << "Error in the tree" << endl;
    else if (root->llink == NULL)
        cerr << "Error in the tree:"
             << " No left subtree to rotate." << endl;
    else
    {
        p = root->llink;
        root->llink = p->rlink; //the right subtree of p becomes
                                //the left subtree of root
        p->rlink = root; 
        root = p;	//make p the new root node
    }
}//end rotateRight

template <class elemT>
void AVLTreeType<elemT>::balanceFromLeft(AVLNode<elemT>* &root)
{
    AVLNode<elemT> *p;
    AVLNode<elemT> *w;

    p = root->llink;   //p points to the left subtree of root

    switch (p->bfactor)
    {
    case -1: 
        root->bfactor = 0;
        p->bfactor = 0;
        rotateToRight(root);
        break;

    case 0:  
        cerr << "Error: Cannot balance from the left." << endl;
        break;

    case 1:  
        w = p->rlink;
        switch (w->bfactor)  //adjust the balance factors
        {
        case -1: 
            root->bfactor = 1;
            p->bfactor = 0;
            break;

        case 0:  
            root->bfactor = 0;
            p->bfactor = 0;
            break; 

        case 1:  
            root->bfactor = 0;
            p->bfactor = -1;
        }//end switch

        w->bfactor = 0;	
        rotateToLeft(p);
        root->llink = p;
        rotateToRight(root);
    }//end switch;
}//end balanceFromLeft

template <class elemT>
void AVLTreeType<elemT>::balanceFromRight(AVLNode<elemT>* &root)
{
    AVLNode<elemT> *p;
    AVLNode<elemT> *w;

    p = root->rlink;   //p points to the left subtree of root

    switch (p->bfactor)
    {
    case -1: 
        w = p->llink;
        switch (w->bfactor)  //adjust the balance factors
        {
        case -1: 
            root->bfactor = 0;
            p->bfactor = 1;
            break;

        case 0:  
            root->bfactor = 0;
            p->bfactor = 0;
            break;

        case 1:  
            root->bfactor = -1;
            p->bfactor = 0;
        }//end switch

        w->bfactor = 0;	
        rotateToRight(p);
        root->rlink = p;
        rotateToLeft(root);
        break;

    case 0:  
        cerr << "Error: Cannot balance from the left." << endl;
        break;

    case 1:  
        root->bfactor = 0;
        p->bfactor = 0;
        rotateToLeft(root);
    }//end switch;
}//end balanceFromRight

template <class elemT>
void AVLTreeType<elemT>::insertIntoAVL(AVLNode<elemT>* &root, 
                                AVLNode<elemT>  *newNode, 
                                bool& isTaller)
{
    if (root == NULL)
    {
        root = newNode;
        isTaller = true;
    }
    else if (root->info == newNode->info)
        cerr << "No duplicates are allowed." << endl;
    else if (root->info > newNode->info) //newItem goes in 
                                         //the left subtree
    {
        
        //RECURSIVE CALL
        insertIntoAVL(root->llink, newNode, isTaller);

        if (isTaller) //after insertion, the subtree grew in height
            switch (root->bfactor)
            {
            case -1:
                balanceFromLeft(root);
                isTaller = false;
                break;

            case 0:  
                root->bfactor = -1;
                isTaller = true;
                break;

            case 1:  
                root->bfactor = 0;
                isTaller = false;
            }//end switch
        }//end if
        else
        {
            insertIntoAVL(root->rlink, newNode, isTaller);

            if (isTaller) //after insertion, the subtree grew in
                          //height
                switch (root->bfactor)
                {
                case -1: 
                    root->bfactor = 0;
                    isTaller = false;
                    break;

                case 0:  
                    root->bfactor = 1;
                    isTaller = true;
                    break;

                case 1:  
                    balanceFromRight(root);
                    isTaller = false;
                }//end switch
        }//end else
}//insertIntoAVL


template<class elemT>
void AVLTreeType<elemT>::insert(const elemT &newItem)
{
    bool isTaller = false;
    AVLNode<elemT>  *newNode;

    newNode = new AVLNode<elemT>;
    newNode->info = newItem;
    newNode->bfactor = 0;
    newNode->llink = NULL;
    newNode->rlink = NULL;

    insertIntoAVL(root, newNode, isTaller);
}


/*
    CONSTRUCTOR & DESTRUCTOR
*/

template <class elemT>
AVLTreeType<elemT>::AVLTreeType()
{   
    root = NULL; 
    
    //add outputfile creation
    outputFile.open("cheilmanlab9.out");
    cout << "output cheilmanlab9.out created" << endl;   
}

template <class elemT>
AVLTreeType<elemT>::~AVLTreeType()
{   
    cout << "Closing ouputfile:::" << endl;
    outputFile.close();
}


/*
    CUSTOM METHODS & IMPLEMENTATIONS
*/

//pass argv[1] as string
template <class elemT>
void AVLTreeType<elemT>::readFromFile(string argFile) {
    
    ifstream datafile;
    string dataList;
    string getData;
    cout << "Reading data from Argument..." << endl << endl;
    outputFile << "Reading data from Argument..." << endl << endl;

    //read data from lab8.dat into array
	datafile.open(argFile);
	if (datafile.is_open()) 
    {
       
       cout << "PreOrder: ";
	   outputFile << "PreOrder: ";
        
       int c = 0;
	   while (datafile >> getData)
	   {
            //outputfile already created on object creation
		  //handle preOrder writeout
            //dataList[c] = getData;
            insert(getData);    //insert info int AVL TREE
            cout << getData;
            outputFile << getData;
            
            //c++;
            counter++;  //nodecounter (private var)
       }
    cout << endl << "------------------------------" << endl;
    outputFile << endl << "------------------------------" << endl;
    
    }
    else{
        cout << "ERROR OPENING DATA FILE!!!. EXITING NO POINT TO LIFE. NO 42..." << endl;
        assert(1 == 2);
    }   
       
	datafile.close();

}



//outputfile writing
//ofstream outputFile (private var)
template <class elemT>
void AVLTreeType<elemT>::writeMethods(){
    //output 
    outputFile.open("cheilmanlab9.out");
    
    //try adrian's method
    //INORDER PRINTOUT
    cout << "InOrder: ";
    outputFile << "InOrder: ";
    InOrder(root);
    cout << endl << "------------------------------" << endl;
    outputFile << endl << "------------------------------" << endl;

    
    //POSTORDER PRINTOUT
    cout << "PostOrder: ";
    outputFile << "PostOrder: ";
    outputPostOrder(root);
    cout << endl << "------------------------------" << endl;
    outputFile << endl << "------------------------------" << endl;
}

//counter (private val)
template<class elemT>
void AVLTreeType<elemT>::showCounter()
{   cout << "TOTAL NODES: " << counter << endl;     }

//inOrder function 
//recursion
template<class elemT>
void AVLTreeType<elemT>::InOrder(AVLNode<elemT>* &root)
{
    if (root->llink != NULL)
    {
        InOrder(root->llink);   
    }   //move to left node

    cout << root->info;
    outputFile << root->info;
    
    if (root->rlink != NULL)
    {
        
        //if right node exists, check for left & right leafs
        if (root->rlink->llink != NULL)
            InOrder(root->rlink->llink);
        //if left leaf, move to it
        
        cout << root->rlink->info;
        outputFile << root->rlink->info;
        
        if(root->rlink->rlink != NULL)  //same as left leaf but with right
            InOrder(root->rlink->rlink);  
            
    }
}

//postorder output traversal for lab 9
//recursively
template<class elemT>
void AVLTreeType<elemT>::outputPostOrder(AVLNode<elemT>* &root)
{
    if (root->llink != NULL)
    {   outputPostOrder(root->llink);    }   //move to node[0]
    
    if (root->rlink != NULL)
    {   outputPostOrder(root->rlink);   }   //move to rightnode
    
    outputFile << root->info;
    cout << root->info;
}

#endif