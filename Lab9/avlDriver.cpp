#include <iostream>
#include <string>
#include <fstream>


#include "avlTree.h"

using namespace std;

/*
CAMERON HEILMAN-AVL TREE DRIVER
COSC 2437
LAB 9
	OBJECTIVES:
		primary: create AVL tree from read data (lab9.dat)
			-print # of nodes
			-print nodes with functions::
				1) inOrder
				2) postOrder
				3) preOrder

*/

int main(int argc, char *argv[])
{
	ifstream datafile;
	char getData;
	string inputfilename = argv[1];
	
	//objects
	AVLTreeType<string> *TreeObj = new AVLTreeType<string>();
	

	if (argc < 1) {

		std::cerr << "EXECUTE ERR!!" << endl << endl;
		std::cerr << "Usage: " << argv[0] << " <filename.dat>" << endl << endl;
		return 1;
	}

	//object creation opens outputfile
	//inputfile specified inside function
	//outputs Pre-Order traversal
	TreeObj->readFromFile(inputfilename);
	
	
	//covers post and inorder traversals
	TreeObj->writeMethods();

	cout << endl << endl << "DONE WITH AVL TRANSACTIONS! " << endl << endl;
	//this is turned to a function inside class
	/*
	//read data from lab8.dat into array
	datafile.open(argv[1]);
	if (datafile.is_open()) {

	while (datafile >> getData)
	{
		//handle preOrder 
		
		//insert node by node into AVL TREE
		Treeobj.insert(getData);
		
			
	}
	
	//debug
	cout << "great. your letters are inserted into the AVL" << endl < endl;
	
	

	datafile.close();
	*/
	delete TreeObj;
	return 0;
}