#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int rVowels(string);

int main()
{

    //use str.find() to step through each
    string input;


    cout << "Hello," << endl << "please enter a string!" << endl;
    getline(cin, input);
    //remove white spaces
    input.erase(std::remove(input.begin(), input.end(), ' '),input.end());

    cout << "# of vowels in your string is " << rVowels(input) << endl;

    return 0;
}


int rVowels(string input)
{
    const string vowels = "aeiou";

    if (input.length() == 0)
        return 0;

    if (vowels.find(input[0]) != string::npos)
        return rVowels(input.substr(1))+1;

    else
        return rVowels(input.substr(1));

}
