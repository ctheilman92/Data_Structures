#include <iostream>
#include <cassert>
#include <string>

#include "queueType.h"





/*
 *                                          *
 *  REFERENCE FOR                           *
 *  ALL MEMBER FUNCTIONS DEFINED IN         *
 *  stackadt.h & stacktype.h                *
 *     ****************************         *
 */


/*
 *
 *
 *
 *
 * THIS SECTION:
 *
 *      -CONSTRUCTOR
 *      -DESTRUCTOR
 *      -COPY CONSTRUCTOR
 *      -copyStack(otherStack) PRIVATE MEMBER FUNCTION
 *      -ASSIGNMENT OPERATOR OVERLOAD
 *
 *
 *
 */



//CONSTRUCTOR WITH SIZE CHOSEN
 template <class Type>
 queueType<Type>::queueType(int queueSize)
 {
 		//default queue size 100
 	if (queueSize <= 0)
 	{
 		cout << "Size of queue must be positive." << endl
 			<< "Creating an array of size 100" << endl;

 		maxQueueSize = 100;
 	}
 	else
 		maxQueueSize = queueSize;		//set max

 	queueFront = 0;		//front of array
 	queueRear = maxQueueSize - 1;	//last subscript in array
 	count = 0;

 	//create array with given size of elements
 	list = new Type[maxQueueSize];

 }

//DESTRUCTOR
template <class Type>
 queueType::~queueType()
 {
 	//deallocate
 	delete [] list;
 }

//COPY CONSTRUCTOR AND OVERLOADED OPERATORS TO COME LATER

 /*
 *
 *
 *
 *
 * BASIC STACK OPERATIONS
 *
 *
 *
 *
 */


 //EMPTY & FULL QUEUE
 template <class Type>
 bool queueType<Type>::isEmptyQueue() const
 {
 	//returns true if count == 0, aka nothing in the array
 	return (count == 0);
 }

 template <class Type>
 bool queueType<Type>::isFullQueue() const
 {
 	//return true if count == size of array
 	return (count == maxQueueSize);
 }


 //INITIALIZATION
 template <class Type>
 void queueType<Type>::initializeQueue()
 {

 	//everything is where it should be
 	queueFront = 0;
 	queueRear = maxQueueSize - 1;
 	count = 0;
 }


 template <class Type>
 Type queueType<Type>::front() const
 {
 	assert(!isEmptyQueue());

 	//if queue is not empty, 
 	//return first element
 	return list[queueFront];
 }

 template <class Type>
 Type queueType<Type::back() const 
 {
 	//return last element in queue
 	assert (!isEmptyQueue());
 	return list[queueRear];
 }


 template <class Type>
 void queueType<Type>::addQueue(const Type& newElement)
 {
 	if (!isFullQueue())
 	{
 		//circular array
 		//use modulus to advance queue rear
 		queueRear = (queueRear + 1) % maxQueueSize;

 		count++;
 		list[queueRear] = newElement;
 	}
 	else
 		cout << "Queue Full! Cannot add element" << endl;

 }

 template <class Type>
 void queueType<Type>::deleteQueue()
 {
 	if (!isEmptyQueue())
 	{

 		count --;
 		//mod to advance queueFront, (circular array)
 		queueFront = (queueFront + 1) % maxQueueSize;
 	}
 }