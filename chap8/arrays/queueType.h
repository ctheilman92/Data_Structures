#ifndef QUEUETYPE_H
#define QUEUETYPE_H

#include <iostream>
#include <cassert>


#include "queueADT.h"

using namespace std;


/*******************
*BUILD FROM QUEUE ADT



*******************/

template <class Type>
class ueueType :public queueADT<Type>
{
private:

	int maxQueueSize;	//store max queue size (size of array)
	int count;
	int queueFront; 	//pointer to the first element
	int queueRear;		//pointer to the back element
	Type *list;			//pointer to array 



public:

		//overloaded operator for easy copying of 2 queues
	const queueType<Type>& operator=(const queueType<Type> &)

	bool isEmptyQueue() const;

	bool isFullQueue() const;

	void initializeQueue();

	Type front() const;

	Type back() const;

		//remove first element from queue
	void deleteQueue();

		//adds element to queue
	void addQueue(const Type& queueElement);

	//CONSTRUCTORS
	queueType(int queueSize = 100);

	//COPY CONSTRUCTOR
	queueType(const queueType<Type>& otherQueue);

	//DESTRUCTOR
	//deallocates array memory
	~queueType()



};


#endif
