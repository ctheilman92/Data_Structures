#ifndef QUEUEADT_H
#define QUEUEADT_H

using namespace std;


/***************
*pure virtual functions to hold regular queue operations
	-isEmptyQueue()
	-isFullQueue() ^^only for arrays^^
	-initializeQueue()
	-front()
	-back()
	-addQueue
	-DeleteQueue
*****************/



template <class Type>
class queueADT
{
public:

		//determines if queue is empty
		//POST: returns true if empty, else false
	virtual bool isEmptyQueue() const = 0;

		//determines if stack is full
		//only for arrays
		//returns true if full
	virtual bool isFullQueue() const = 0;

		//initialize to an empty state
	virtual  oid initializeQueue() = 0;

		//returns the first element in queue
		//pre: queue exists & is not empty
	virtual Type front() const = 0;

		//returns last element in queue
	virtual Type back() const = 0;

		//adds new element to queue
		//pre: queue must exist & is not full
	virtual void addQueue (const Type& queueElement) = 0;

		//deletes the FIRST element in queue
	virtual void DeleteQueue() = 0;
};



#endif 