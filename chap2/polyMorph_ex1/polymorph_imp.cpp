#include<iostream>
#include<string>
#include "polymorph.h"
using namespace std;



/*
 * DATETYPE FUNCITONS
 */

//CONSTRUCTOR
dateType::dateType(int month, int day, int year)
{
    //call setdate function which parameters are passed
    //from other class?
    setDate(month, day, year);
}

//for dateType functions
void dateType::setDate(int month, int day, int year)
{
    dMonth = month;
    dDay = day;
    dYear = year;
}

//next 3 are datetype get functions for private vars
int dateType::getDay() const
{
    return dDay;
}

int dateType::getMonth() const
{
    return dMonth;

}

int dateType::getYear() const
{
    return dYear;
}

//dateType print date function
void dateType::printDate() const
{
    cout << dMonth << "-" << dDay << "-" << dYear;
}

/*
 * PERSONALINFOTYPE FUNCTIONS
 */

//constructor, also creates objects name & bday
//ONCE the personalInfoType object is created (*personalInfoType student;)
//personType name object is created
//dateType bDay object is created
personalInfoType::personalInfoType(string first, string last,
                                   int month, int day, int year, int ID)
        : name(first, last), bDay(month, day, year)
{
    personID = ID;
}

void personalInfoType::setPersonalInfo(string first, string last,
                                       int month, int day, int year, int ID)
{
    name.setName(first,last);
    bDay.setDate(month,day,year);
    personID = ID;
}

void personalInfoType::printPersonalInfo() const
{
    name.print();
    cout << "'s date of birth is ";
    bDay.printDate();
    cout << endl;
    cout << "and personal ID is " << personID;
}
