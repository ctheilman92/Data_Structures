#ifndef POLYMORPH_H
#define POLYMORPH_H


/*
 * this iwll focus on creating 3 classes.
 * 1. datetype
 * 2. persontype
 * 3. personalinfotype
 *
 * personal infotype constructor is used to create persontype and datetype objects
 */

class personType
{


};

class dateType
{
public:
    void setDate(int month, in day, int year);
    //function to set date
    //dMonth, dDay, and dYear are member vars set
    //dMonth = month. etc.

    int getDay() const;
    //return day
    int getMonth() const;
    //return month
    int getYear() const;

    void printDate() const;
    //function to print mm-dd-yyyy.

    dateType(int month = 1, int day = 1, int year = 1900);
    //constructor to set date
    //will be created in personalinfotype constructor

private:
    int dMonth;
    int dDay;
    int dYear;
};

class personalInfoType
{
public:
    void setPersonalInfo(string first, string last, int month, int day, int year, int ID);
    //function to set the personal info
    //member vars are set according to parameters
    //dMonth = month...etc.
    //personID = ID

    void printPersonalInfo() const;
    //function to print personal info

    personalInfoType(string first = "", string last = "",
                     int month = 1, int day = 1, int year = 1900,
                     int ID = 0);
    //CONSTRUCTOR
    //vars set according to parameters
    //dMonth = month...etc.
    //personID = ID
    //if no values are specified, default values are loaded

private:
    personType name;
    //name specified in persontype class
    dateType bDay;
    //bday var from datetype class
    int personID;
};

#endif // POLYMORPH_H

