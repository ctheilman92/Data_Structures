#include <iostream>
using namespace std;


/*
 * this program shows implicit copy constructor
 * and how to use it to copy an object
 */


class person {
public:
    int age;

    explicit person(int a)
        : age(a)
    {
    }

};


int main()
{
    person Tim(10);
    person Mark(20);


    //close time
    person Tim2 = Tim;

    cout << Tim.age << " " << Mark.age << " " << Tim2.age << endl << endl;

    Tim.age = 50;
    cout << Tim.age << " " << Mark.age << " " << Tim2.age << endl << endl;
return 0;
}
