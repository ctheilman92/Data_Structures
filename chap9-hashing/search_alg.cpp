#include <iostream>
using namespace std;


/*
+++++++++++++++++++++++++++++
REVIEW OF SEARCH ALGORITHMS
+++++++++++++++++++++++++++++
*/



/*
	
	SEQUENTIAL SEARCH:
		-linear
		-bad for large lists

	::avg key comparisons = (n+1)/2
*/



/*
	
	BINARY SEARCH:
		-divides comparisons in half by splitting list by 2
		-LIST MUST BE ORDERED	

		::avg search alg= 2log(base2)n -3
*/

//mockup
template <class elemType
int orderedArrayListType<elemType>::binarySearch
										(const elemType& item) const
{
	int first = 0;
	int last = length-1;
	int mid;

	bool found = false;

	while (first <= last && !found)
	{
		mid = (first+last)/2;

		if (list[mid] == item)
			found = true;

		else if (list[mid] > item)
			last = mid-1;

		else
			first = mid + 1;
	}

	if (found)
		return mid;
	else
		return -1;


}



/*

	FOR INSERTION
		-use algorithm similar to binary search to find proper place
		-if item is not in list
			-call insertAt();
			*/

template <class elemType>
void orderedArrayListType<elemType>::insertOrd(const elemType& item)
{
	int first = 0;
	int last = length-1;
	int mid;

	bool found = false;

		//if array is empty
	if (length == 0)
	{
		list[0] = item;		//insert item at the front 
		length++;
	}
	else if (length == maxSize)
	{
		cerr << "Cannot insert into a full list." << endl;

	}
	else	//standard insertion method uses alg.
	{
		while (first <= last && !found)
		{
			mid = (first+last)/2;

			if (list[mid] == item)
				found = true;
			else if (list[mid] > item)
				last = mid-1;

			else	
				first = mid+1;		//if mid < item
		}

		if (found)
			cerr << "the item is already in list" 
				<< endl << "Duplicates not allowed" << endl;

		else	//if item not in list
		{
				//if last searched is less than item
			if (list[mid] < item)
				mid++;

			insertAt(mid, item);
		}

	}
}