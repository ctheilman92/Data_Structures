#ifndef H_ORDEREDARRAYLISTTYPE
#define H_ORDEREDARRAYLISTTYPE

#include <iostream>
#include <iomanip>

#include "arrayListType.h";

using namespace std;


template <class elemType>
class orderedArrayListType: public arrayListType<elemType>
{
public:

	//OVERRIDE THESE FUNCTIONS
	//so to keep list in order after calls
	void insertAt(int location, elemType& insertItem);

	void insertEnd(const elemType& insertItem);


	//combines binary search + insrtAt() method from
	//derived class
	void insertOrd(const elemType&);

	int binarySearch(const elemType& item) const;

	//default constructor
	orderedArrayListType(int size = 100);
};



//DEFINITIONS
/*
++++++++++++++++++++++++
++++++++++++++++++++++++
++++++++++++++++++++++++
++++++++++++++++++++++++
*/


template <class elemType
int orderedArrayListType<elemType>::binarySearch
										(const elemType& item) const
{
	int first = 0;
	int last = length-1;
	int mid;

	bool found = false;

	while (first <= last && !found)
	{
		mid = (first+last)/2;

		if (list[mid] == item)
			found = true;

		else if (list[mid] > item)
			last = mid-1;

		else
			first = mid + 1;
	}

	if (found)
		return mid;
	else
		return -1;


}



/*

	FOR INSERTION
		-use algorithm similar to binary search to find proper place
		-if item is not in list
			-call insertAt();
			*/

template <class elemType>
void orderedArrayListType<elemType>::insertOrd(const elemType& item)
{
	int first = 0;
	int last = length-1;
	int mid;

	bool found = false;

		//if array is empty
	if (length == 0)
	{
		list[0] = item;		//insert item at the front 
		length++;
	}
	else if (length == maxSize)
	{
		cerr << "Cannot insert into a full list." << endl;

	}
	else	//standard insertion method uses alg.
	{
		while (first <= last && !found)
		{
			mid = (first+last)/2;

			if (list[mid] == item)
				found = true;
			else if (list[mid] > item)
				last = mid-1;

			else	
				first = mid+1;		//if mid < item
		}

		if (found)
			cerr << "the item is already in list" 
				<< endl << "Duplicates not allowed" << endl;

		else	//if item not in list
		{
				//if last searched is less than item
			if (list[mid] < item)
				mid++;

			insertAt(mid, item);
		}

	}
}


#endif