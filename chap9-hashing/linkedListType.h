#ifndef H_LinkedListType
#define H_LinkedListType

#include <iostream>
#include <cassert>

using namespace std;

//Definition of the node

template <class Type>
struct nodeType
{
	Type info;
	nodeType<Type> *link;
};

template <class Type>
class linkedListIterator
{
public:
    linkedListIterator();
    //Default constructor
    
    linkedListIterator(nodeType<Type> *ptr);
    //constructor with params

    Type operator*();
    //overload dereference operator
    
    linkedListIterator<Type> operator++();
    //Overload the preincrement operator.
    
    //won't be using
    bool operator==(const linkedListIterator<Type>& right) const;

    //won't be using
    bool operator!=(const linkedListIterator<Type>& right) const;

    
private:
    nodeType<Type> *current; //pointer to point to the current
    //node in the linked list
};


template <class Type>
linkedListIterator<Type>::linkedListIterator()
{
    current = NULL;
}

template <class Type>
linkedListIterator<Type>::
linkedListIterator(nodeType<Type> *ptr)
{
    current = ptr;
}

template <class Type>
Type linkedListIterator<Type>::operator*()
{
    return current->info;
}

template <class Type>
linkedListIterator<Type> linkedListIterator<Type>::operator++()
{
    current = current->link;
    
    return *this;
}

template <class Type>
bool linkedListIterator<Type>::operator==(const linkedListIterator<Type>& right) const
{
    return (current == right.current);
}

template <class Type>
bool linkedListIterator<Type>::operator!=(const linkedListIterator<Type>& right) const
{
    return (current != right.current);
}

template <class Type>
class linkedListType
{
public:
    const linkedListType<Type>& operator= (const linkedListType<Type>&);
    //Overload the assignment operator.
    
    void initializeList();
    //Initialize the list to an empty state.
    
    bool isEmptyList() const;
    //Function to determine whether the list is empty.
    
    void print() const;
    //Function to output the data contained in each node.

    int length() const;
    //Function to return the number of nodes in the list.

    
    void destroyList();
    //destroy
    
    Type front() const;
    //return first el
    
    Type back() const;
    //return last el
    
    virtual bool search(const Type& searchItem) const = 0;
    //search virtual for derived class
    
    virtual void insertFirst(const Type& newItem) = 0;

    
    virtual void insertLast(const Type& newItem) = 0;

    
    virtual void deleteNode(const Type& deleteItem) = 0;

    
    linkedListIterator<Type> begin();
    //Function to return beginning iterator

    
    linkedListIterator<Type> end();
    //Function to return ending iterator
    
    linkedListType();
    //default constructor

    linkedListType(const linkedListType<Type>& otherList);
    //copy constructor
    
    ~linkedListType();
    //destructor

    
    int count;
    nodeType<Type> *first;
    nodeType<Type> *last;
    
private:
    void copyList(const linkedListType<Type>& otherList);

};

template <class Type>
bool linkedListType<Type>::isEmptyList() const
{
    return (first == NULL);
}

template <class Type>
linkedListType<Type>::linkedListType() //default constructor
{
    first = NULL;
    last = NULL;
    count = 0;
}

template <class Type>
void linkedListType<Type>::destroyList()
{
    nodeType<Type> *temp;
    //occupied by the node
    while (first != NULL)
    {
        temp = first;
        first = first->link; //move to next node
        delete temp;   //deallocate the memory occupied by temp
    }
    
    last = NULL; //initialize last to NULL; first has already
    count = 0;
}

template <class Type>
void linkedListType<Type>::initializeList()
{
	destroyList(); //if the list has any nodes, delete them
}

template <class Type>
void linkedListType<Type>::print() const
{
    nodeType<Type> *current; //pointer to traverse the list
    current = first;

    while (current != NULL) //while more data to print
    {
        cout << current->info << " ";
        current = current->link;
    }
}

template <class Type>
int linkedListType<Type>::length() const
{
    return count;
}

template <class Type>
Type linkedListType<Type>::front() const
{
    assert(first != NULL);
    
    return first->info; //return the info of the first node
}

template <class Type>
Type linkedListType<Type>::back() const
{
    assert(last != NULL);
    
    return last->info; //return the info of the last node
}

template <class Type>
linkedListIterator<Type> linkedListType<Type>::begin()
{
    linkedListIterator<Type> temp(first);
    
    return temp;
}

template <class Type>
linkedListIterator<Type> linkedListType<Type>::end()
{
    linkedListIterator<Type> temp(NULL);
    
    return temp;
}

template <class Type>
void linkedListType<Type>::copyList(const linkedListType<Type>& otherList)
{
    nodeType<Type> *newNode; //pointer to create a node
    nodeType<Type> *current; //pointer to traverse the list
    
    if (first != NULL) //if the list is nonempty, make it empty
        destroyList();
    
    if (otherList.first == NULL)
    {
        first = NULL;
        last = NULL;
        count = 0;
    }
    else
    {
        current = otherList.first; //current points to the
        //list to be copied
        count = otherList.count;
        
        //copy the first node
        first = new nodeType<Type>;  //create the node
        
        first->info = current->info; //copy the info
        first->link = NULL;

        last = first;              //make last point to the
        //first node
        current = current->link;     //make current point to
        //the next node
        
        //copy the remaining list
        while (current != NULL)
        {
            newNode = new nodeType<Type>;  //create a node
            newNode->info = current->info; //copy the info
            newNode->link = NULL;       //set the link of
            //newNode to NULL
            last->link = newNode;  //attach newNode after last
            last = newNode;        //make last point to
            //the actual last node
            current = current->link;   //make current point
            //to the next node
        }
    }
}

template <class Type>
linkedListType<Type>::~linkedListType() //destructor
{
    destroyList();
}//end destructor

template <class Type>
linkedListType<Type>::linkedListType
(const linkedListType<Type>& otherList)
{
   	first = NULL;
    copyList(otherList);
}//end copy constructor


template <class Type>
const linkedListType<Type>& linkedListType<Type>::operator=
(const linkedListType<Type>& otherList)
{
    if (this != &otherList) //avoid self-copy
    {
        copyList(otherList);
    }//end else
    
    return *this;
}

#endif


