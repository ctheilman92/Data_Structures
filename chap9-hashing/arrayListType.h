#ifndef H_ARRAYLISTTYPE
#define H_ARRAYLISTTYPE

#include <iostream>
#include <iomanip>
using namespace std;

/*********************
	this header copied from
	Chap 9 (hashing and search algorithms)
*********************/


template <class elemType>
class arrayListType{

public:

		//OVERLOADED COPY OPERATOR
	const arrayListType<elemType>& operator=(const arrayListType<elemType>&);

	bool isEmpty() const;

	bool isFull() const;

	int listSize() const;

	int maxListSize() const;

	void print() const;

	bool isItemAtEqual(int location, const elemType& item) const;

	void insertAt(int location, elemType& insertItem);

	void insertEnd(const elemType& insertItem);

	void removeAt(intem location);

	void retrieveAt(int location, elemType& retItem) const;

	void replaceAt(int location, const elemType& repItem);

	void clearList();

	int seqSearch(const elemType& item) const;

	void insert(const elemType& insertItem);

	void remove(const elemType& removeItem);



	//////constructors & destructors///////

		//default
	arrayListType(int size = 100);

		//copy constructor
	arrayListType(const arrayListType<elemType>& otherList);

	~arrayListType();



protected:
	elemType * list;	//array to hold list elements
	int length;			//length of list
	int maxSize;		//max size of list

};


//unordered sequential search!
	//returns -1 if unsuccessful search
tmeplate <class ElemType>
int arrayListType<elemType>::seqSearch(const elemType& item) const
{
	int loc;
	bool found = false;

	for (loc = 0; loc < length, loc++)
	{
		if (list[loc] == item)
		{
			found = true;
			break;
		}
	}

	if (found)
		return loc;

	else
		return -1;

}


#endif