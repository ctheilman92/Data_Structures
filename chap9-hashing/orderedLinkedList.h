#ifndef H_orderedListType
#define H_orderedListType

#include "linkedListType.h"

using namespace std;



template <class Type>
class orderedLinkedList: public linkedListType<Type>
{   
public:

    //to avoid syntax error
	using linkedListType<Type>::count;
    	using linkedListType<Type>::first;
   	using linkedListType<Type>::last;

    bool search(const Type& searchItem) const;
    //Function to determine whether searchItem is in the list.

    
    void insert(const Type& newItem);
    //Function to insert newItem in the list.

    void insertFirst(const Type& newItem);
    //Function to insert newItem at the beginning of the list.

    
    void insertLast(const Type& newItem);
    //Function to insert newItem at the end of the list.

    
    void deleteNode(const Type& deleteItem);
    //Function to delete deleteItem from the list.

};

template <class Type>
bool orderedLinkedList<Type>::search(const Type& searchItem) const
{
    bool found = false;
    nodeType<Type> *current; //pointer to traverse the list
    
    current = first;  //start the search at the first node
    
    while (current != NULL && !found)
        if (current->info >= searchItem)
            found = true;
        else
            current = current->link;
    
    if (found)
        found = (current->info == searchItem); //test for equality
    
    return found;
}//end search


template <class Type>
void orderedLinkedList<Type>::insert(const Type& newItem)
{
    
    cout << "1     "  << newItem << "  ";
    nodeType<Type> *current; //pointer to traverse the list
    nodeType<Type> *trailCurrent; //pointer just before current
    nodeType<Type> *newNode;  //pointer to create a node
    
    bool  found;
    
    newNode = new nodeType<Type>; //create the node
    newNode->info = newItem;   //store newItem in the node
    newNode->link = NULL;      //set the link field of the node
    //to NULL
    
    if (first == NULL)  //Case 1
    {
        first = newNode;
        last = newNode;
        count++;
    }
    else
    {
        current = first;
        found = false;
        
        while (current != NULL && !found)
        {
            if (current -> info == newItem)
            {
                return;
            }
            
            if (current->info >= newItem)
                found = true;
            else
            {
                trailCurrent = current;
                current = current->link;
            }
        }
        
        if (current == first)      //Case 2
        {
            newNode->link = first;
            first = newNode;
            count++;
        }
        else                       //Case 3
        {
            trailCurrent->link = newNode;
            newNode->link = current;
            
            if (current == NULL)
                last = newNode;
            
            count++;
        }
    }
}

template<class Type>
void orderedLinkedList<Type>::insertFirst(const Type& newItem)
{
    insert(newItem);
}

template<class Type>
void orderedLinkedList<Type>::insertLast(const Type& newItem)
{
    insert(newItem);
}

template<class Type>
void orderedLinkedList<Type>::deleteNode(const Type& deleteItem)
{

    nodeType<Type> *current; //pointer to traverse the list
    nodeType<Type> *trailCurrent; //pointer just before current
    bool found;
    
    if (first == NULL) //Case 1
        cout << "debug: list emptyp" << endl << endl;
    else
    {
        current = first;
        found = false;
        
        while (current != NULL && !found)  //search the list
            if (current->info >= deleteItem)
                found = true;
            else
            {
                trailCurrent = current;
                current = current->link;
            }
        
        if (current == NULL)
            return;

        else
            if (current->info == deleteItem) //is equal?

            {
                if (first == current)       //Case 2
                {
                    first = first->link;
                    
                    if (first == NULL)
                        last = NULL;
                    
                    delete current;
                }
                else                         //Case 3
                {
                    trailCurrent->link = current->link;
                    
                    if (current == last)
                        last = trailCurrent;
                    
                    delete current;
                }
                count--;
            }
            else                            //Case 4
                return;
    }
}

#endif


