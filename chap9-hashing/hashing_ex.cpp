#include <iostream>
#include <string>

using namespace std;



/******************

this will sample some code from chapter 9

*******************/

static int keyLength;



//
//	MODULAR DIVISION HASH h(X);
//
int hashFunction(char *insertKey, int keyLength)
{
	int sum = 0;

		//convert key into an integer & divide by the size of the table
		//get remainder to ID address in hash table
	for (int j = 0; j < keyLength; j++)
		sum = sum + static_cast<int>(insertKey[j]);

	return (sum % HTSize);
}


/*
COLLISION RESOLUTION algorithms 
	1) open addressing (closed hashing)
	2) chaining (open hashing)
*/




int main(int argc, char *argv[])
{
	
//OPEN ADDRESSING :: LINEAR PROBING
//
//		if primary slot for key insertion is full, 
//				sequentially search for next open slot
//	mode division./


//call hashfunc
hIndex = hashFunction(insertKey);
found = false;


//LINEAR PROBE I
	//precondition: slot must be filled for there to be a colission.
	while (HT[hIndex] != emptyKey && !found)
	{

		if (HT[hIndex].key == key)				//if collision is not detected
			found = true;						

		else	
			hIndex = (hIndex + 1) % HTSize;		//sequential probe

		if (found)	
			cerr << "Duplicate items are not allowed." << endl;

		else
			hIndex = newItem;
	}


//RANDOM PROBE I
	//may need to check for validity!!!!!!!!!!!!!!!!!!!!!!!
	//precondition: collision must be detected
	r1 = 2;
	r2 = 5;
	r3 = 8;

	while (HT[hIndex] != emptyKey && !found)
	{
		if (HT[hIndex].key == key)
			found = true;

		else
			hIndex = (hIndex + r1) % HTSize;

			if (!found)
				hIndex = (hIndex + r2) % HTSize;
			else
				found = true;

			if(!found)
				hIndex = (hIndex + r3) % HTSize;
			else
				found = true;

		if (found)
			cerr < "DUPLICATE items not allowed." << endl;

		else
			hIndex = newItem;
	}






	//FOR QUADRATIC PROBE I
		//may bug out if HTSize is prime
		//computes (t + i^2)%HTSize
	int inc = 1;
	int pCount = 0;
	hIndex = hashFunction(insertKey);

	while (HT[hIndex] !emptyKey
			&& HT[hIndex] != insertKey
			&& pCount < HTSize /2)
	{
		pCount++;
		hIndex = (hIndex + inc) % HTSize;
	}
	if (HT[hIndex] == emptyKey)
		HT[hIndex] = newItem;

	else if (HT[hIndex] == insertKey)
		cerr << "Error: no duplicates are allowed." << endl;
	else
		cerr << "Error: the table is full. " 
			<< "Unable to resolve collision." << endl;

	return 0;
}