#include <iostream>
#include <fstream>
#include <string>
#include <cassert>


#include "arrayListType.h"



using namespace std;

/*************************
**************************

	~Owner:	Cameron Heilman~
::DRIVER FOR COSC 2437 LAB 8::
	read data from lab8.dat into standard array
	sort using selection sort from arrayListType.h
		-count loop iterations & record moves
		-display output
		-output file to cheilmanLab8.out
		
	*copied list of UNSORTED data will be used as placeholder 
	so user can see results of multiple algorithms*

**************************
*************************/

int main(int argc, char *argv[])
{
	int tries = 0;
	int datarow;
	ifstream datafile;
	int algChoice;
	char cont = 'y';

	//initialize array stack of size 70;
	//not dynamic. clearlist(); instead
	arrayListType<int> objRay(70);	//normal
	//arrayListType<int> *objRay = new arrayListType<int>(70);									//dynamic (test if overloaded operator works)


	if (argc < 1) {

		std::cerr << "EXECUTE ERR!!" << endl << endl;
		std::cerr << "Usage: " << argv[0] << " <filename.dat>" << endl << endl;
		return 1;
	}



	//read data from lab8.dat into array
	datafile.open(argv[1]);
	if (datafile.is_open())
	{
		
		//read data in
		while (datafile >> datarow) {
				//just a validation. shouldnt be full
			if (!objRay.isFull())
				objRay.insert(datarow);	//insert row item
			
		}
		
		cout << "UNSORTED ARRAY!" << endl << "++++++++++++++++++++++++++++" << endl << endl;
		objRay.print();
		
		//create UNSORTED copy list obj
		//copy constructor call. dynamically?
		arrayListType<int> objCopy(objRay);
		//arrayListType<int> *objCopy = new arrayListType<int>(*objRay);
		
		
		while (tolower(cont) == 'y') {
		
			//only works one iteration. WHY?!
			if (objRay.isEmpty()) {	//if user continues. objRay is cleared at the end. so copy the list again
				
				cout << endl << endl << "great, unsorting the array now." << endl;
				objRay = objCopy;
				cout << "UNSORTED ARRAY!" << endl << "++++++++++++++++++++++++++++" << endl;
				//objRay->print();
				objCopy.print();	//debug
				cout << endl;
			}
		
			//switch case to choose what sorting to be 
			//switch menu
			cout << endl << endl << "Select a Sorting algorithm (1-4)" << endl
				<< "=============================" << endl << "=============================" << endl
				<< "(1)	Selection Sort" << endl
				<< "(2)	Quick Sort" << endl
				<< "(3)	Insertion Sort" << endl
				<< "(4)	QUIT WHILE YOU'RE AHEAD" << endl
				<< "=============================" << endl << "=============================" 
				<< endl << endl << "Choice: ";
			cin >> algChoice;
		
		
			//validation
			while(algChoice <= 0 || algChoice > 4) {
				if (tries == 3) {
					cout << "you suck, I quit..." << endl;
					return 1;
				}
				else {
					cout << "invalid answer" << endl << "Choice: ";
					cin >> algChoice;
				}
				tries++;
			}
		
			switch(algChoice) {
				case 1:
					cout << "Selection sort selected!....." << endl << endl;
					objRay.selectionSort();
					cout << endl << "***********SORTED*********" << endl << endl;
					cout << endl << endl << "SORTED ARRAY!" << endl << "+++++++++++++++++++++++++++" << endl;
					objRay.print();  
					objRay.analysis();   
					break;
				case 2:
					cout << "Quick sort selected!..... " << endl;
					objRay.quickSort();
					cout << endl << "***********SORTED*********" << endl;
					cout << endl << endl << "SORTED ARRAY!" << endl << "+++++++++++++++++++++++++++" << endl;
					objRay.print();
					objRay.analysis();
					break;
				case 3:
					cout << "Insertion sort selected!....." << endl;
					objRay.selectionSort();
					cout << endl << "***********SORTED*********" << endl;
					cout << endl << endl << "SORTED ARRAY!" << endl << "+++++++++++++++++++++++++++" << endl;
					objRay.print();
					objRay.analysis();
					break;
				case 4:
					cout << endl << "exiting. thanks for playing." << endl;
					//delete objCopy;
					//delete objRay;
					return 0;
					break;
			}

		
			//clear the list. iterate through until they quit.
			objRay.clearList();
			cout << endl << endl << "WOULD YOU LIKE TO CONTINUE??????" << endl
				<< "(y/n): ";
			cin >> cont;                                                                                                                    
		}
		
	}
	datafile.close();
	//delete objRay;




	return 0;
}