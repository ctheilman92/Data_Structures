#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

#include "linkedQueue.h"
#include "linkedStack.h"

using namespace std;

/***************

	:Cameron Heilman 
	:COSC 2437	 -LAB 6 

	___DRIVER CPP___
	setting up the same way of execution as lab6
	file-in: lab6.txt
	file-out: cheilmanLab6.out

	usage:-> $./palendrone lab6.txt


*****************/


int main(int argc, char *argv[])
{
	string line;
	ifstream file;
	bool matched = true;

	//originals
	linkedStackType<string> s;
	linkedQueueType<string> q;
	//linkedStackType<string> scop;

	if (argc < 2) {        
		// Tell the user how to run the program
        std::cerr << "EXECUTE ERR!!!!!!!!!" << endl;
        std::cerr << "Usage1: " << argv[0] << " 'FILENAME' " << endl << "OR" << endl;
        std::cerr << "Usage2: " << argv[0] << " A+B*C (infix expression) " << endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;		
	}

	file.open(argv[1]);
	if (file.is_open())
	{
		//read file line by line
		//use iterator to step through and assign characters to stack & queue
		while (file >> line)
		{

			cout << "word:   *(" << line << ")*" << endl << "====================================" << endl;

			for (int it = 0; it < line.length(); it++)
			{
				//so this still stays a string. 
				//instead of messing with damn templates
				string iter(1,tolower(line[it]));

				s.push(iter);
				q.addQueue(iter);

				cout << "LOG:> " << iter << " added to stack & queue" << endl;
			}

			//READ forward through queue & backward through stack.
			while (!q.isEmptyQueue() && !s.isEmptyStack())
			{
				if(q.front() != s.top())
					matched = false;

				q.deleteQueue();
				s.pop();
			}
			

			if (!matched)
				cout << endl << "*(" << line << ")*  -->>NOT A PALINDROME<<--" << endl << endl << endl;
			
			else
				cout << endl << "*(" << line << ")*  -->>IS A PALINDROME<<--" << endl << endl << endl;
			
			
			

			s.initializeStack();
			q.initializeQueue();
		}
		file.close();
	}

	//reads fine
	//FINISH OBJECTIVE
	else
	{
		cout << endl << "Argument is Not a File..." << endl;
		cout << "reading from argument!: " << endl << endl;

		line = argv[1];
		cout << "word:   *(" << line << ")*" << endl << "======================================" << endl;

		for (int it = 0; it < line.length(); it++)
		{
			string iter(1, tolower(line[it]));
			s.push(iter);
			q.addQueue(iter);
			cout << iter << " has been pushed to stack!" << endl;
		}

		while (!s.isEmptyStack() && !q.isEmptyQueue())
		{
			if (s.top() != q.front())
				matched = false;

			q.deleteQueue();
			s.pop();	
		}

		if (!matched)
			cout << endl << "  *(" << line  << ")*  ->>NOT A PALINDROME<<-" << endl << endl << endl;

		else
			cout << endl << "  *(" <<  line << ")*  ->>IS A PALINDROME<<-" << endl << endl << endl;

	}


	return 0;	
}