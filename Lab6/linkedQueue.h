#ifndef LINKEDQUEUE_H
#define LINKEDQUEUE_H

#include <iostream>
#include <cassert>
#include <string>

#include "queueADT.h"

using namespace std;


/***********************
	CAMERON HEILMAN COSC 2437 _LLQ IMPLEMENTATION & HEADER

	LINKED LIST IMPLEMENTATION 
	FOR QUEUES.

	-USE SAME QUEUE ADT FOR BASIC OPERATIONS

	**BOTH CLASS HEADER & IMPLEMENTATION ARE IN ONE FILE**

************************/


//define node
template <class Type>
struct qnodeType
{
	Type info;
	qnodeType<Type> * link;
};


template <class Type>
class linkedQueueType :public queueADT<Type>
{
private:
	qnodeType<Type> *queueFront;		//pointer to front of list
	qnodeType<Type> *queueRear;		//pointer to back of list

public:

	//overloaded operator
	const linkedQueueType<Type>& operator=
					(const linkedQueueType<Type>&);

	bool isEmptyQueue() const;

		//only for arrays (not implemented here)
		//only listed because QUEUEADT
	bool isFullQueue() const;

	void initializeQueue();

	Type front() const;

	Type back() const;

	void addQueue(const Type& queueElement);

	void deleteQueue();

	//queue read checker
	void iterator();

	//CONSTRUCTORS & DESTRUCTORS
	//
	//

	linkedQueueType();	//DEFAULT

	//COPY CONSTRUCTOR
	linkedQueueType(const linkedQueueType<Type>& otherQueue);

	~linkedQueueType();	//DESTRUCTOR
};





 /*
 *
 *
 *
 *
 * THIS SECTION:
 *
 *      -CONSTRUCTOR
 *      -DESTRUCTOR
 *      -COPY CONSTRUCTOR
 *      -copyStack(otherStack) PRIVATE MEMBER FUNCTION
 *      -ASSIGNMENT OPERATOR OVERLOAD
 *
 *
 *
 */

//DEFAULT CONSTRUCTOR *check*
template <class Type>
linkedQueueType<Type>::linkedQueueType()
{
	queueFront = NULL;	//set both pointers to null & done
	queueRear = NULL;
}


template <class Type>
linkedQueueType<Type>::~linkedQueueType()
{
	initializeQueue();
}




 /*
 *
 *
 *
 *
 * BASIC STACK OPERATIONS
 *
 *
 *
 *
 */


//EMPTY & FULL QUEUES
template <class Type>
bool linkedQueueType<Type>::isEmptyQueue() const
{
 	//returns true if pointer is set to null (NOT INITIALIZED)
  	return (queueFront == NULL);
}

template <class Type>
bool linkedQueueType<Type>::isFullQueue() const
{
	//NO SET SIZE OF LIST
	//always return false
	return false;
}


//initialization
template <class Type>
void linkedQueueType<Type>::initializeQueue()
{
	//new node
	qnodeType<Type> * temp;

		//if there are elements leftover in queue
		//delete them
	while (queueFront != NULL)
	{
		temp = queueFront;	//set temp to front of queue
		queueFront = queueFront->link;	//advance pointer

		delete temp;	//deallocate
	}

	queueRear = NULL;	//set rear to NULL
}


//ADDING ELEMENTS
template <class Type>
void linkedQueueType<Type>::addQueue(const Type& newElement)
{
	//new node
	qnodeType<Type> * newNode;
	newNode = new qnodeType<Type>;	//created

	newNode->info = newElement;
	newNode->link = NULL;			//set to null for now

	if (queueFront == NULL)		//if queue is empty
	{
			//set both pointers to new node
		queueFront = newNode;
		queueRear = newNode;
	}
	else
	{
			//connect nodes, then reset queueRear to the newest node
		queueRear->link = newNode;
		queueRear = queueRear->link;
	}
}

//DELETING THE FIRST ELEMENT 
//REMEMBER FIFO!!!
template <class Type>
void linkedQueueType<Type>::deleteQueue()
{
	qnodeType<Type> * temp;

	if (!isEmptyQueue())
	{
		temp = queueFront;	//set temp to front
		queueFront = queueFront->link;	//advance queueFront

		delete temp;	//deallocate

		//after deletion, if queue is empty, set rear to null too
		if (queueFront == NULL)
			queueRear = NULL;

	}
	else
		cout << "EMPTY QUEUE: cannot remove first element." << endl;
}

template <class Type>
Type linkedQueueType<Type>::front() const
{
	//IF NOT NULL, RETURN NODE INFO
	assert(queueFront != NULL);
	return queueFront->info;
}

template <class Type>
Type linkedQueueType<Type>::back() const
{
	//IF NOT NULL, RETURN NODE INFO
	assert(queueRear != NULL);
	return queueRear->info;
}


//queue read checker
template <class Type>
void linkedQueueType<Type>::iterator() 
{
	qnodeType<Type> * temp;

	if (!isEmptyQueue())
	{

		temp = queueFront;
		cout << front();

		while (temp->link != queueRear)
		{
			temp = temp->link;
			cout << temp->info;
		}
		if (temp->link == queueRear)
			cout << back();
	}

}

#endif