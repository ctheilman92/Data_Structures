#ifndef LINKEDSTACK_H
#define LINKEDSTACK_H
#include <cassert>
#include <iostream>

#include "stackadt.h"
/*
*
*   LINKED LIST STACK TYPE
*   DYNAMICALLY ALLOCATED MEMORY IS MORE EFFICIENT THAN ARRAYS
*   
*   UTILIZE STL: #include <stack>
*   http://www.dreamincode.net/forums/topic/57497-stl-stack/
*
*/


template <class Type>
struct nodeType
{
    Type info;
    nodeType<Type> *link;
};

template <class Type>
class linkedStackType :public stackADT<Type>
{
private:

     //pointer to the stack linked list
    nodeType<Type> *stackTop;  

    //private function called by COPY CONSTRUCTOR
    void copyStack(const linkedStackType<Type>& otherStack);



public:
    //overload assignment operator
    const linkedStackType<Type>& operator=
                                (const linkedStackType<Type>&);

    //function determines if stack is empty
    bool isEmptyStack() const;

    //if stack is full
    //THIS FUNCTION IS NOW IRRELEVANT!!!
        //--WHY? linked lists DON'T have a max stacksize.
        //this is only here because the ADT base class defines it
    bool isFullStack() const;

    //initialize stack to empty state.
    //POST: stackTop = NULL && elements are removed if any
    void initializeStack();

    //POST: newItem to the top of the stack
    //PRE: stack exists and is NOT full
    void push(const Type& newItem);

    //function to return the top element of the stack.
    //PRE: stack exists and is not empty
    //POST: if stack is empty. terminate program
    Type top() const;

    //removes top element in stack
    void pop();





    //default constructor
    //POST: stackTop == NULL;
    linkedStackType();

    //COPY CONSTRUCTOR
    linkedStackType(const linkedStackType<Type>& otherStack);

    //destructor
    //POST: all elements are removed from stack
    ~linkedStackType();



};





/*
 *
 *
 *
 *
 * THIS SECTION:
 *
 *      -CONSTRUCTOR
 *      -DESTRUCTOR
 *      -COPY CONSTRUCTOR
 *      -copyStack(otherStack) PRIVATE MEMBER FUNCTION
 *      -ASSIGNMENT OPERATOR OVERLOAD
 *
 *
 *
 */


//DEFAULT CONSTRUCTOR
template <class Type>
 linkedStackType<Type>::linkedStackType()
 {
    stackTop = NULL;
 }

 //COPY CONSTRUCTOR
 //passes other stack as PARAMETER
 template <class Type>
 linkedStackType<Type>::linkedStackType(const linkedStackType<Type>& otherStack)
 {
    stackTop = NULL;

    //CALL COPYSTACK FUNCTION & PASS OTHER STACK AS ARG
    copyStack(otherStack);
 }

//DESTRUCTOR
 template <class Type>
 linkedStackType<Type>::~linkedStackType()
 {
    initializeStack();
 }

//OVERLOADED OPERATOR
template <class Type>
 const linkedStackType<Type>& linkedStackType<Type>::operator=
                        (const linkedStackType<Type>& otherStack)
{
    if (this != &otherStack)    //avoid self copy
        copyStack(otherStack);

    return *this;
}









 /*
 *
 *
 *
 *
 * BASIC STACK OPERATIONS
 *
 *
 *
 *
 */

 template <class Type>
 void linkedStackType<Type>::initializeStack()
 {
    //temp pointer to delete the node
    nodeType<Type> *temp;


    //while stack is not empty
    while (stackTop != NULL)
    {
        temp = stackTop;    //set pointer to top element

        stackTop = stackTop->link;  //move stackTop to next node.

        delete temp;    //deallocate memory 
    }
 }


 template <class Type>
 bool linkedStackType<Type>::isEmptyStack() const
 {
    return(stackTop == NULL);
 }


//don't really need this
 template <class Type>
 bool linkedStackType<Type>::isFullStack() const
 {
    //LINKED LIST IS NEVER FULL!!!
    return false;
 }



 //PUSH FUNCTION
 //REFERENCE 7-12 p.420
 //what to do:
    //1. create newNode pointer.
        //a.set newNode->info 
        //b.set newNode->link = stackTop
    //2. move stackTop to newNode position
template <class Type>
 void linkedStackType<Type>::push(const Type& newItem)
 {
    nodeType<Type> *newNode;    //1
    newNode = new nodeType<Type>;
    
    newNode->info = newItem;    //1.a
    newNode->link = stackTop;   //1.b

    stackTop = newNode;         //2

 }

//TOP (RETURN TOP ELEMENT)
//straight forward. still use assert function
 template <class Type>
 Type linkedStackType<Type>::top() const
 {
    assert(stackTop != NULL);   //if stack is empty. KILL the program

    return stackTop->info;
 }


 //POP (DELETE) top-element from stack
 //WHAT TO DO:
    //1. create temp node
        //a. point temp node to stackTop position
        //b. move stacktop to next element in the list (stackTop = stackTop->link)
    //2. delete temp node
template <class Type>
void linkedStackType<Type>::pop()
{
    nodeType<Type> *temp;               //1


    if (stackTop != NULL)
    {
        temp = stackTop;                //1.a

        stackTop = stackTop->link;      //1.b


        delete temp;                    //2
    }

    else
        cout << "can't remove from an empty stack." << endl << endl;
}

//COPY-STACK PRIVATE MEMBER FUNCTION
//MAKE A COPY OF THE STACK
//passes in the already existing stack as a parameter
//copies to the newly made stack
template <class Type>
void linkedStackType<Type>::copyStack
                    (const linkedStackType<Type>& otherStack)
{
    //set node pointers
    nodeType<Type> *newNode, *current, *last;

//CHECK IF THIS IS THE FIRST ELEMENT TO BE COPIED
    if (stackTop != NULL)
        initializeStack();

    if (otherStack.stackTop == NULL)
        stackTop = NULL;

    //ELSE IF THIS STACK IS NULL, AND OTHER STACK ISN'T NULL
    //continue copying data in.
    else
    {
        current = otherStack.stackTop;  //current points to the stack to be copied

        //copy stackTop element of the stack
        stackTop = new nodeType<Type>;  //creates node

        stackTop->info = current->info; //copy info of other stack's stackTop
        stackTop->link = NULL;          //link field is null
        last = stackTop;                //last is same as stackTop
        current = current->link;        //move to next element to be copied 
    

        //copy remaining stack
        while (current != NULL)
        {
            newNode = new nodeType<Type>;

            newNode->info = current->info;  //copy data from current element
            newNode->link = NULL;           //points out
            last->link = newNode;           //last node = most recently copied node
            current = current->link;        //move to next element to be copied
        }
    }
}


#endif // LINKEDSTACK_H