#include "stacktype.h"


/*
 *                                          *
 *  REFERENCE FOR                           *
 *  ALL MEMBER FUNCTIONS DEFINED IN         *
 *  stackadt.h & stacktype.h                *
 *                                          *
 */


/*
 *
 *
 *
 *
 * THIS SECTION:
 *
 *      -CONSTRUCTOR
 *      -DESTRUCTOR
 *      -COPY CONSTRUCTOR
 *      -copyStack(otherStack) PRIVATE MEMBER FUNCTION
 *      -ASSIGNMENT OPERATOR OVERLOAD
 *
 *
 *
 *
 */


//CONSTRUCTOR
//sets maxStackSize from user input
//if not specified by user, default size = 100
//sets stackTop to 0, & creates appropriate array[maxStackSize]
template <class Type>
stackType::stackType(int stackSize)
{
    if (stackSize <= 0)
    {
        cout << "size MUST be positive. " << endl;
        cout << "creating an array of size 100" << endl;

        maxStackSize = 100;
    }
    else
        maxStackSize = stackSize;   //set passed value to private maxStacksize

    stackTop = 0;                   //set stackTop to 0
    list = new Type[maxStackSize];  //create array

}

//DESTRUCTOR
//deallocates memory of array(the stack)
//set stackTop to 0
template <class Type>
stackType::~stackType()
{
    delete [] list;
}


//COPY CONSTRUCTOR
//when stackType object is created by:
//passing another stackType object as a parameter
//list is copied through copyStack(otherStack) function!
template <class Type>
stackType::stackType(const stackType<Type>& otherStack)
{
    list = NULL;

    //call private function
    copyStack(otherStack);
}

//USE THIS TO IMPLEMENT COPY-CONSTRUCTOR
//OVERLOAD THE = OPERATOR
template <class Type>
void stackType<Type>::copyStack(const stackType<Type>& otherStack)
{
    //if a list does exist. delete it.
    delete [] list;

    maxStackSize = otherStack.maxStackSize;
    stackTop = otherStack.stackTop;
    list = new Type[maxStackSize];

    //copy elements
    for (int j = 0; j < stackTop; j++)
    {   list[j] = otherStack.list[j];   }

}

//OVERLOADED '=' OPERATOR
//returns the stackType OBJ using *this pointer
template <class Type>
const stackType<Type>& stackType<Type>::
    operator=(const stackType<Type>& otherStack)
{
    if (this != &otherStack) //avoid self-copy
        copyStack(otherStack);


    return *this;
}

/*
 *
 *
 *
 *
 * BASIC STACK OPERATIONS
 *
 *
 *
 *
 */

template <class Type>
void stackType<Type>::initializeStack()
{
    stackTop = 0;
}

template <class Type>
bool stackType<Type>::isEmptyStack() const
{
    //returns T, if stackTop is 0
    return(stackTop == 0);
}


template <class Type>
bool stackType<Type>::isFullStack() const
{
    //if we hit maxStackSize, return TRUE
    return(stackTop == maxStackSize);
}

template <class Type>
void stackType<Type>::push(const Type& newItem)
{
    //see 7-8. p. 404
    //1. store newItem in the array[p] indicated by stackTop
    //2. increment stackTop


    //overflow error check
    if (!isFullStack())
    {
            //ESSENTIAL OPERATIONS OF FUNCTION
        list[stackTop] = newItem;
        stackTop++;
    }

    else
        cout << "STACK IS FULL. SORRY!" << endl << endl;
}


template <class Type>
Type stackType<Type>::top() const
{
    //terminate program if stack is empty
    assert(stackTop != 0);

            //since stackTop = 1 starts list[0]
    return list[stackTop -1];

}

template <class Type>
void stackType<Type>::pop()
{
    //see 7-9. P.406
    //JUST DECREMENT stackTop

    //underflow check
    if (!isEmptyStack())
        stackTop--;
    else
        cout << "STACK IS EMPTY NOTHING TO POP" << endl << endl;

}




