#include <iostream>
#include "stacktype.h"
using namespace std;

/*
 *  THIS IS THE TEST DRIVER FOR STACKADT.H & STACKTYPE.H
 *  given ex. 7-1
 *  p. 410
 */


//whats up with this function dec?
void testCopyConstructor(stackType<int> otherStack);


int main()
{
    //obj dec.
    stackType<int> stack(50);
    stackType<int> copyStack(50);
    stackType<int> dummyStack(100);

    //initialize stack.
    //push 3 numbers to array[0],[1],and [2]
    //stackTop = 3
    stack.initializeStack();
    stack.push(23);
    stack.push(45);
    stack.push(38);

    //copy stack using overloaded operator
    copyStack = stack;


    cout << "the elements of copyStack: ";

    while (!copyStack.isEmptyStack())   //print copyStack and pop element as we go
    {
        cout << copyStack.top() << "  ";
        copyStack.pop();
    }
    cout << endl;


    return 0;
}


void testCopyConstructor(stackType<int> otherStack)
{
    if (!otherStack.isEmptyStack())
        cout << "otherStack is not empty." << endl
             << "the Top element of the otherstack: "
             << otherStack.top() << endl;
}
