#ifndef STACKADT_H
#define STACKADT_H
#include <iostream>
#include <cassert>


//*   UTILIZE STL: #include <stack>
//*   http://www.dreamincode.net/forums/topic/57497-stl-stack/



template <class Type>
class stackADT
{
public:
    //pure: initialize stack to empty state
    //postcondition: stack is empty
    virtual void initializeStack() = 0;

    //pure: determines if stack is empty
    virtual bool isEmptyStack() const = 0;

    //pure: determine if stack is full
    virtual bool isFullStack() const = 0;

    //adds newItem to the stack
    virtual void push(const Type& newItem) = 0;

    //returns top stack element
    //pre: stack is not empty
    //post: if stack is empty, program terminates
            //else returned top element
    virtual Type top() const = 0;

    //remove top element from stack
    //pre: stack exists and not empty
    //post
    virtual void pop() = 0;
};

#endif // STACKADT_H
