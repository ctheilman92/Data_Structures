#include <iostream>
#include <cassert>
#include "linkedStack.h"
using namespace std;

void testCopy(linkedStackType<int> OStack);

int main()
{
	
	linkedStackType<int> stack;
	linkedStackType<int> otherStack;
	linkedStackType<int> newStack;

		//add elements
	stack.push(34);
	stack.push(43);
	stack.push(27);


		//overloaded assignment operator
	newStack = stack;

	cout << "After assignment operator, newStack: " 
		<< endl;

	//output stack elements
	while (!newStack.isEmptyStack())
	{
		//read top element
		cout << newStack.top() << endl;

		//remove element after read
		newStack.pop();
	}//newstack is now empty after read

	//copy stack to Otherstack
	otherStack = stack;

	cout << "Testing the copy constructor." << endl;

	//local testCopy function
	testCopy(otherStack);

	cout << "After copy constructor, otherStack: " << endl;

	while (!otherStack.isEmptyStack())
	{
		cout << otherStack.top() << endl;
		otherStack.pop();
	}



return 0;
}