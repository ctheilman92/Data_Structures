#ifndef clockType_H
#define clockType_H

/*
	Cameron Heilman
	COSC2437.301
	Fall 2015
	Source code from Malik, chapter 1
*/

class clockType
{
public:
        clockType(int hours, int minutes, int seconds);
            //constructor with parameters which set the time
            //checks whether values are valid. if not assign 0

        clockType();
            //default constructor with parameters
            //time is set to 00:00:00.
            //postcondition: hr = 0; min = 0; sec = 0

	void setTime(int hours, int minutes, int seconds);
			//function to set the time
			//postcondition: hr = hours; min = minutes...etc.
			//checks whether values are valid, if not set to 0
	
	void getTime(int& hours, int& minutes, int& seconds) const;
			//Function to return the time	
			//Postcondition: hours = hr; minutes = min; etc....
			
	void printTime() const;
			//function to print the time
			
	void incrementSeconds();
			//function to increment by 1 seconds
			//postcondition: time incremented 1 seconds
			// if before-increment is 23:59:59, the time is reset to 00:00:00
			
	void incrementMinutes();
			//function to increment by 1 minutes
			//postcondition: time incremented 1 minute
			//	if before-increment time is 23:59:53, time is reset to 00:00:53
			

	void incrementHours();
			//function to increment by 1 hours	
			//postcondition: time incremented 1 hours
			// 	if before-increment is 23:45:53, time is reset to 00:45:53
	
	bool equalTime(const clockType& otherClock) const;
			//Function to compare the two times
			//Postcondition: returns true if this time is equal to otherClock
			//otherwise false.

private:
	int hr;
	int min;
	int sec;

};

#endif
