#include <iostream>
#include <iomanip>
using namespace std;

/*
output this:
****
***
**
*
*
**
***
****
============
*/





int flag(int n)
{
    int i = n;

    if (n == 0)
    {
        while(i < 4)
        {
            cout << "*";
            i++;
        }
        cout << endl;
        flag(n+1);
    }

    else
    {
        while(i > 0)
        {
            cout << "*";
            i--;
        }
        cout << endl;

        flag(n-1);
     }
}






int main()
{

	int n = 4;

	flag(n);

	return 0;
}
