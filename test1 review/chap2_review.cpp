#include <iostream>
#include <string>
using namespace std;



/*
 * review for chapter 2
 * concepts covered:
 *
 * -inheritance
 *      -access
 *      -overriding functions
 *      -overloading functions
 * -overloaded operators (*******)
 * -this pointers (******)
 * -friend functions
 * -function overloading
 * -templates (*********)
 */


/*********************
 * inheritance   
    -PUBLIC ACCESSOR: public base-class members are public derived-class members      *
    -PROTECTED ACCESSOR: base-class members are protected derived members
        ::accessible through member functions and friend functions of derived class
    -PRIVATE ACCESSOR: base-class private members are hidden from derived-class
        ::accessible for derived-class functions THROUGH base-class public & protected members
 *********************/


class baseClass{
public:

    //default constructor
    //only initializes the obj
    baseClass();

    //constructor with params
    baseClass(int);

    void print();    //prints values for base class

private:
    int x, y, z;
    int a;


protected:
    int d;
    void print();

};


class otherClass : public baseClass{

public:

    //default constructor
    otherClass()
    {
        b = 0;
        c = 0;
    }

    otherClass(int,int,int);

    //OVERRIDING (REDEFINED) function
    //baseclass has the same function name. but his
    int print();

    int Sum(int, int, int);
    int geta();                 //since a is protected in the base class, it's considered PRIVATE in the inherited class
    int getb();                 //a,b & c are all private members to this class
    int getc();



private:
    int b, c;

};



/*******
 * override function definitions
 * ******/

void baseClass::print()
{   cout << "here is printing of base class stuff" << endl; }

//overriden derived class print function
void otherClass::print()
{   cout << "here is printing of the base class stuff" << endl; }


//baseclass constructor with param
baseClass::baseClass(int aa)
{
    b = aa;
}

//derived constructor, passing a derived and set variable "a"
otherClass::otherClass(int aa, int bb, int cc)
    : baseClass(aa)
{
    //a is already designated.
    //we can therefore focus on the inclass variables
    //since int a was derived
    b = bb;
    c = cc;

}
//  ---> HERE WOULD BE THE CONSTRUCTION OF BOTH OBJECTS <--
baseClass base(5);    //initialize int a = 5 for baseclass object
otherClass other(4, 5, 6);   //initializes a=4,b=5, c=6 for other object

base.print();
//print a = 5;

other.print();
//print a = 4, b = 5, c = 6





/*******************
********************//*******************
********************//*******************
********************//*******************
********************//*******************
********************//*******************
********************//*******************
********************/
/*******************
********************/






/****************

*THIS 
    -*this pointer refers to the object as a whole
    -return *this can return the entire object
    -just like other pointers
        :return *this <-- returns the members stored in the object
        :return this <-- returns the ADDRESS of the object itself
*****************/

        /*box member variables:
            private: int a, int b;

         */

Box operator= (const Box b2)
{
    this = b2;

}


//book example function
//x & y are objects of class test
//return type is designated test
test test::setVar(int v1)
{
    privateVar = v1;
    ......
    return *this;
}

//in main
//copies all data members of x-obj into y-obj
y = x.function();
/*================================================*/

//class member function prototype of an object that returns *this
test& setVar1(int v1);

//NOTICE
//the return type of the function is a reference to the class. 
//makes sense
//objects declared
test obj1;
test obj2;

//call would be like just the same. 
//sets member variables && calls a reference to the object itself 
obj1.setVar1(5);
obj2.setVar(10);
/*================================================*/
/*A BETTER EXAMPLE*/

class Box
{
   public:
      // Constructor definition
      Box(double l=2.0, double b=2.0, double h=2.0)
      {
         cout <<"Constructor called." << endl;
         length = l;
         breadth = b;
         height = h;
      }
      double Volume()
      {
         return length * breadth * height;
      }
      int compare(Box box)
      {
        //refers to the address location of the parent object that calls this function
        //to access Volume() function.
         return this->Volume() > box.Volume();
      }
   private:
      double length;     // Length of a box
      double breadth;    // Breadth of a box
      double height;     // Height of a box
};

int main(void)
{
   Box Box1(3.3, 1.2, 1.5);    // Declare box1
   Box Box2(8.5, 6.0, 2.0);    // Declare box2

   //
   if(Box1.compare(Box2))
   {
      cout << "Box2 is smaller than Box1" <<endl;
   }
   else
   {
      cout << "Box2 is equal to or larger than Box1" <<endl;
   }
   return 0;
}
/*DIAGNOSE
    compare function passes object (box) as data-type *Box*




/*OUTPUT
    Constructor Called.
    constructor Called
    Box2 is equal to or larger than Box1
*/


/****************

OVERLOADING OPERATORS

*****************/
//continuing with box class
class Box
{
private:
    double length, breadth, height;

public:
    //default constructor
    Box();
    //consturctor instead
    Box(double ll, double bb, double hh)
    {
        length = 0.0;
        breadth = 0.0;
        height = 0.0;
    }

    double getVol()
    {    return length * breadth * height;   }


    //overloading the '+' operator
    //passes other box obj as param while referring to parent class obj with this-
    Box operator+(const Box& b)
    {
        Box obj;    //final object after adding both objects together
        obj.length = this->length + b.length;
        obj.breadth = this->breadth + b.breadth;
        obj.height = this->height + b.height;

        //return new object 
        return box;
    }
};

int main()
{
    //declare boxes and set private variables for each through params
    Box b1(6.0, 7.0, 5.0);
    Box b2(12.0, 5.0, 14.0);
    Box b3; //final box undeclared

    double volume = 0.0;    //object volume storage

    //box 1 volume
    volume = b1.getVol();
    cout << "volume of Box1: " << volume << endl;

    //box 2 volume
    volume = b2.getVol();
    cout << "volume of Box2: " << volume << endl;

    //add objects
    //OVERLOADED OPERATOR
    b3 = b1 + b2;

    //box 3 volume
    volume = b3.getVol();
    cout << "volume of Box3: " << volume << endl;

    return 0;
}


/****************

UNARY OPERATOR OVERLOADING
    -unary operators like !, ++, --, and - usually appear left of the object.
    -don't require two variables, just the operator and object.

*****************/










/*******************
********************//*******************
********************//*******************
********************//*******************
********************//*******************
********************//*******************
********************//*******************
********************/
/*******************
********************/






/****************

TEMPLATES YAAAAYY   

    :GENERIC PROGRAMMING
    :INDEPENDENT OF ANY PARTICULAR TYPE
    :CREATES GENERIC CLASSES & FUNCTIONS

*****************/

//TEMPLATE FUNCTION DEFINITION
    //template <class type> ret-type function-name(parameters);
template <class type> void function(int, int);
{
    //holds name of the datatype used by function
    //can be used inside the function body
}

/*examples of function definition*/

/*T replaces the following:
    1. return data type
    2. passed-param data types
*/
template <typename T>
inline T const& Max(T const& a, T const& b)
{
    return a < b ? b:a
}
int main()
{

    int i = 39;
    int j = 20;
    cout << "Max(i, j): " << Max(i, j) << endl; 
    //function passes integers for the template

    double f1 = 13.5; 
    double f2 = 20.7; 
    cout << "Max(f1, f2): " << Max(f1, f2) << endl; 
    //function passes floats

    string s1 = "Hello"; 
    string s2 = "World"; 
    cout << "Max(s1, s2): " << Max(s1, s2) << endl; 
    //funciton passes strings

   return 0;
}



//CLASS TEMPLATES

//definition: specified when class is instantiated.
template <class type> 
class Box
{
public:
    void print();


}

class plane
{
public:

}

type::print()
{
    
    cout << "x = " << type::x;

}










