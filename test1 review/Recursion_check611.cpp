#include <iostream>
using namespace std;


/*
	excercise 6-11 from book.
	CALCULATE THE OUTPUT OF THE FOLLOWING CODE
*/

int fun(int);


int main()
{

	cout << "f(0): " << fun(0) << endl;
	cout << "f(1): " << fun(1) << endl;
	cout << "f(3): " << fun(3) << endl;
	cout << "f(5): " << fun(5) << endl;

return 0;
}

int fun(int x)
{
	if (x == 0)
		return 2;

	else if (x == 1)
		return 3;

	else
		return (fun(x-1) + fun(x-2));
}

/*TEST CASES

function(0):	2
function(1):	3
function(2):	5
function(5):	21
*/

