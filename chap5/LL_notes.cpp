#include<iostream>
using namespace std;




/*
notes on link lists
this cpp file is non-compilable
get over it.
*/



struct nodeType
{
    int info;
    nodeType *link;
};

//sets head initialized to first node
//always set head to point to first node in list
nodeType *head;

//current used to traverse the link list
nodeType *current = head;

//processing heads
while (current != NULL)
{
    //outputs data stored in that node
    cout << current->info;
    //each iteration moves head point to the next node
    current = current->link;
}

//insertion/deletion
nodeType *head, *p, *q, *newNode;

// if p->link = head->link->link
//or p = the next node in the list

//creates new node
newNode = new nodeType;
newNode->info = 50; //int 50 is stored in new node.

//this order is IMPERATIVE
//if mixed up it will break your list
newNode->link = p->link;
p->link = newNode;


//if 2 pointers are used
//order doesn't matter
//PLACES NEW NODE IN BETWEEN P & Q
newNode->link = q;
p->link = newNode;

//to delete a node
//removing the pointer link by skipping over it and destroying the link
//but memory is still occupied
p->link = p->link->link;

//using 2 pointers is more functional again
q = p->link;        //sets q to the node after p
p->link = q->link   //p then hops over q to the next node
delete q;           //node q is deleted

----------------------------------------------------------------------------------]
BUILDING LL's'
-------------------------------------------------------------------------
//use 3 pointers to build
nodeType *first, *last, *newNode;
//process data
int num;

//initialize pointers
first = NULL;
last = NULL;

cin >> num;
newNode = new nodeType; //allocate memory for new node
newNode->info = num;    //fills data of node
newNode->link = NULL;   //initialize pointer link to null

//if list isn't initialized
if(first == NULL)
{
    //sets first pointer to new node pointer
    //first->info = num
    //last->info = num as well
    //since there is only 1 node
    first = newNode;
    last = newNode;
}

else    //else if list isn't empty
{
    last->link = newNode;   //inserts new node at the end of list
    last = newNode;         //sets last to point to last node in the list
}


    /*
     * just like arrays, ordered link lists take less compiling time
     * when searching for things.
     * using virtual functions for many operations reduces it as well
     */

