#include <iostream>
using namespace std;

/*
 * this helps understand the idea of passing in strings
 * into main from command line
 *
 *
 * when running in terminal.
 * execute with extra strings.
 * **
 * ****./argtest string1 string2 string3
 * ****
 * **
 *
 */



int main(int argc, char *argv[])
{

    cout << "have " << argc << " arguments: " << endl;
    for (int i = 0; i < argc; i++)
    {
        cout << argv[i] << endl;
    }

return 0;
}
