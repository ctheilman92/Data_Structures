#include <iostream>
using namespace std;


//a&b are the bases. 
//n is the counter used to return
int rFib(int a, int b, int n);

int main()
{
    int firstNum, secNum, nth;
    cout << "here is a list of the fib sequence " << endl << "********************" << endl << endl;
    cout << "1,1,2,3,5,8,13,21,34....etc. " << endl << "n0,n1,n2,n3,n4,n5,n6,n7,n8,n9" << endl              <<endl;
    cout << "enter the first fibonacci number: ";
    cin >> firstNum;
    
    cout << endl << "enter 2nd fibonacci number: ";
    cin >> secNum;
    
    cout << endl << "enter position of the destined fibonacci number: ";
    cin >> nth;
    
    cout << "number at position " << nth << " is : " << endl;
    rFib(firstNum, secNum, nth);
    
return 0;
}


int rFib(a, b, n)
{
    if (n == 1)
        return a;
    
    else if (n == 2)
        return b;
        
    else
        return rFib(a, b, n-1) + rFib(a, b, n-2);
}
        