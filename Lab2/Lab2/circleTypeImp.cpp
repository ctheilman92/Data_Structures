#include<iostream>
#include<iomanip>
#include "circleType.h"
using namespace std;



/*
 * x-y coordinates need to be stored, displayed and manipulated easily.
 * as well passing through simple functions to calculate circle circumference and other data.
 * class circleType stores radius and center of the circle (x-y coordinates)
 * DERIVE circleType from pointType.
 * pointType = Base class
 * circleType = Derived
 * circle operations::: set radius, print radius, calculate area, calculate circumference
 */

/*PROTECTED VARS:
 * double radius;
 */

/*
 * 10 bonus points to use an overloaded operator
 */


void circleType::print() const
{




        //double getx, gety;
	cout << "CIRCLE CONTENTS" << endl << "==========================" << endl;

        //print coordinates using get functions. otherwise they won't be used.
        cout << endl << "CENTER: (" << pointType::getX() << ", " << pointType::getY() << ")";
        cout << endl << "RADIUS: " << getRadius();
        cout << endl << "CIRCUMFERENCE: " << getCircumference();
        cout << endl << "AREA: " << getArea();
        cout << endl;
}

void circleType::setRadius(double r)
{
    radius = r;
}

double circleType::getRadius() const
{
    return radius;
}

double circleType::getCircumference() const
{
    //2pi(r)
   return (2.0 * 3.14 * radius);
}


double circleType::getArea() const
{

            //area = pi(r)^2

            return(3.14 * (radius*radius));
}


circleType::circleType(double x, double y, double r)
{
            xCoordinate = x;
            yCoordinate = y;
            radius = r;
}

void circleType::calcMenu() const
{
            cout << endl << "==========================================" << endl
                 << "1: change Center coordinates" << endl
                 << "2: Change X-coordinate only" << endl
                 << "3: Change Y-coordinate only" << endl
                 << "4: Change the radius" << endl
                 << "==========================================" << endl << endl;
}
