#include<iostream>
#include<iomanip>
#include "pointType.h"
using namespace std;


/*
 * x-y coordinates need to be stored, displayed and manipulated easily.
 * as well passing through simple functions to calculate circle circumference and other data.
 * class circleType stores radius and center of the circle (x-y coordinates)
 * DERIVE circleType from pointType.
 * pointType = Base class
 * circleType = Derived
 * circle operations::: set radius, print radius, calculate area, calculate circumference
 */


/*PROTECTED VARS:
 * double xcoordinate
 * double ycoordinate
 */

/*
 * 10 bonus points to use an overloaded operator
 */

void pointType::operator=(const pointType &pObj)
{
    xCoordinate = pObj.getX();
    yCoordinate = pObj.getY();
}

void pointType::setPoint(double x, double y)
{
    xCoordinate = x;
    yCoordinate = y;
}

void pointType::print() const
{
    cout << endl << "POINT OBJECT COORDINATES: (" << getX() << ", " << getY() << ")";
    cout << endl;
}

double pointType::getX() const
{
    return xCoordinate;
}

double pointType::getY() const
{
    return yCoordinate;
}

pointType::pointType(double x, double y)
{
    cout << "point object created " << endl;
}

