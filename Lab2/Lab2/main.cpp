#include<iostream>
#include<iomanip>
#include "circleType.h"
#include "pointType.h"
using namespace std;

/*
 * x-y coordinates need to be stored, displayed and manipulated easily.
 * as well passing through simple functions to calculate circle circumference and other data.
 * class circleType stores radius and center of the circle (x-y coordinates)
 * DERIVE circleType from pointType.
 * pointType = Base class
 * circleType = Derived
 * circle operations::: set radius, print radius, calculate area, calculate circumference
 */

/*
 * 10 bonus points to use an overloaded operator
 */


int main()
{
    //local vars
    double x, y, r;

    //switch statement vars
    char ans = 'n';
    int choice;


    cout << endl << endl << endl << "welcome to the coordinate circle calculator" << endl;
    cout << "===========================================" << endl << endl;


   //create pointType object default values
   pointType pObj;

   //user set coordinates
   cout << "please enter coordinates" << endl;
   cout << "X: ";
   cin >> x;
   cout << endl << "Y: ";
   cin >> y;


   //default set x, y to 0 until user input
   pObj.setPoint(x,y);
   pObj.print();


    //create circle object, and copy pointtype object data over to child object
    circleType cObj;
    cObj.pointType::operator=(pObj);

    /* DEBUG EDIT OUT
    cout << endl << "::::::DEBUG:::::::" << endl << "p.Obj.print() = ";
    pObj.print();
    cout << endl << "getX() = " << pObj.getX();
    cout << endl << "getY() = " << pObj.getY();
    cout << endl << "local x = " << x;
    cout << endl << "local y = " << y;
    */


   //set radius
   cout << endl << endl << "Coordinates loaded into the circle object. " << endl << "===============================" << endl;
   cout << "please enter a radius for the circle" << endl << "Rad: ";
   cin >> r;
   cObj.setRadius(r);

   //call circleType::print();
   cObj.print();



   //changing values, and calculations
   cout << "\n\nWould you like to change any values for your circle?" << endl << "(y/n)? ";
           cin >> ans;
           cout << endl;
           while (ans == 'y' || ans == 'Y')
           {
                //call added function displaying menu and switch cases
               cObj.calcMenu();
               cout << "Choose: ";
               cin >> choice;

               switch(choice)
               {
                    case 1 :
                        {
                           cout << "New X: ";
                           cin >> x;
                           cout << endl << "New Y: ";
                           cin >> y;
                           pObj.setPoint(x, y);
                           cObj.pointType::operator=(pObj);

                        }
                        break;

                    case 2 :
                        {
                            cout << endl << "Enter a new X-coordinate";
                            cin >> x;
                            pObj.setPoint(x, y);
                            cObj.pointType::operator=(pObj);


                        }
                        break;

                    case 3 :
                        {
                            cout << endl << "Enter a new Y-coordinate";
                            cin >> y;
                            pObj.setPoint(x, y);
                            cObj.pointType::operator=(pObj);

                        }
                        break;

                    case 4 :
                        {
                            cout << endl << "Set new radius: ";
                            cin >> r;
                            cObj.setRadius(r);
                        }
                        break;
               }

                cout << endl << "UPDATED CIRCLE VALUES!" << "================================" << endl;
                cObj.print();


                cout << "change more values? " << endl << "(y/n)? ";
                cin >> ans;
           }

        cout << endl << endl << "ending summary" << endl << endl;
        cObj.print();

return 0;
}
