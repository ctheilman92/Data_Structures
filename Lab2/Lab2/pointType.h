//Class pointType

#ifndef H_PointType
#define H_PointType

/*
 * 10 bonus points to use an overloaded operator
 */

class pointType
{
public:
    void setPoint(double x, double y);
    void print() const;
    double getX() const;
    double getY() const;
    void operator=(const pointType&);
    pointType(double x = 0.0, double y = 0.0);

protected:
    double xCoordinate;
    double yCoordinate;
};

#endif
