//Class circleType

#ifndef H_CircleType
#define H_CircleType

#include "pointType.h"


/*
 * 10 bonus points to use an overloaded operator
 */
 
class circleType: public pointType
{
public:
	void print() const;
	void setRadius(double r);
	double getRadius() const;
	double getCircumference() const;
	double getArea() const;
        void calcMenu() const;
	circleType(double x = 0.0, double y = 0.0, double r = 0.0);

protected:
	double radius;
};

#endif
