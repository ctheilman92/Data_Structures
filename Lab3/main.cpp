﻿#include<iostream>
#include<fstream>
#include "orderedLinkedList.h"
using namespace std;


/*
 * OPERATIONS PERFORMED
 * 	1. create a linked list from data stored in lab3.dat
 *	2. read out data to lab3.out
 * 	3. update the linked list using data stored in lab3up.dat
 * 	4. produce audit trail of transactions when updating
 *
 * 	*print*
 * 		-original list and num of elements
 * 		-audit trail of updates, key, and audit message
 * 			:key = 0: deleted
 * 			:key = 1: added
 * 		-updated list and num of elements
 *
 * 	*use templates for classes*
 * 	*USE ADT's
 * 	2 header files merging implementation code:
 * 		1> LinkedListType.h
 * 			:merge with linkedListIterator.h
 * 			:make class linkedListType an ADT with 11 functions
 * 			:derive LinkedListIterator from linkedListType
 * 		2> OrderedLinkedList.h
 * 			:derive from linkedlistType
 *
	*use basic read/write file operations in main
*/



int main()
{
    //read numbers as strings for input output file operations.
    //initialize list object with type int
    //LinkedListType<int> initList, secList;
    orderedLinkedList<int> initList;
    int num;
    int nodecount;


    cout << "Here we will be reading in the data, from lab3.dat and assigning it to the ordered link list"
        << endl << endl;

    //BEGIN READ FROM lab3.dat, insert link list nodes
    ifstream initfile;
    initfile.open("lab3.dat");
    if (initfile.is_open()) //if opens correctly
    {
        while (initfile >> num)
        {
            //read data into num var, and pass it to insert function
            //initfile >> num;
            cout << endl << "DEBUG: " << num << " added to link list...." << endl << endl;
            initList.insert(num);   //instantiated insert function (virtual)
        }

    }
    else
        cout << endl << "could not open designated file" << endl << endl;

    //print link list so far
    cout << "++++++++++++++++++" << "Here is the populated link list " << endl << "++++++++++++++++++" << endl;

    //print updated list
    nodecount = initList.length();
    cout << "# of Nodes: " << nodecount;
    cout << endl << "printed list: ";
    initList.print();

    cout << endl << endl << "-------------------------" << endl;
    cout << "great. now lets continue to update the list!" << endl;


    /*
     *
     * update list using list 2.
     * designate 2 variables for the column to populate updated link list
     * create function to print audit trail
     *
     * OUTPUT SAMPLE:
     *  0   5876    DELETE Successful
     *  1   5343    INSERT Failed (DUPLICATE key)
     *  0   7451    DELETE Failed (NOT in list)
     *  1   1000    INSERT Successful
     *  1   1984    INSERT Failed (OVERFLOW)
     *
     */
    int col1, col2;

    ifstream updatefile;
    updatefile.open("lab3up.dat");
    while(updatefile >> col1 >> col2)
    {
        //use the 2nd link list for updated values.
        //upList.insert(col2);

        //DELETE KEYS
        if (col1 == 0)
        {
            //if col2 is in the list, proceed
            if (initList.search(col2))
            {
                    //delete col2 from list, then check again in list.
                    initList.deleteNode(col2);
                    if (!initList.search(col2))
                        cout << "AUDIT: " << col1 << "  " << col2 << " DELETE SUCCESSFUL" << endl;

            }
            else
                cout << "AUDIT: " << col1 << "  " << col2 << " DELETE FAILED:   (NOT IN LIST)" << endl;

        }
        //INSERT KEYS
        else
        {
            //if col2 already exists in list
            if (initList.search(col2))
                cout << "AUDIT: " << col1 << "  " << col2 << "  INSERT FAILED:  (DUPLICATE KEYS)" << endl;


            //else try to insert it
            else
            {
                initList.insert(col2);
                if(initList.search(col2))
                    cout << "AUDIT: " << col1 << "  " << col2 << "  INSERT SUCCESSFUL" << endl;
            }
        }
    }

/*
 * TO DO STILL
 * ++++++++++++++
 * 1. insert else if for overflow failure of insert column!
 * 2. write new list to file!
 * 3. display new updated list!
 * ++++++++++++++
 */

    cout << endl << "=====================================" << endl << "great. here is the new updated link list: " << endl << "=====================================" << endl;
    nodecount = initList.length();
    cout << "# of nodes: " << nodecount << endl;
    cout << "printed list: " << endl;
    initList.print();


    //WRITE NEW FILE
    ofstream finalfile;
    finalfile.open("lab3.out");


    //declare linkListIterator obj;
    linkedListIterator<int> listIt;
    listIt = initList.begin();  //overloaded '=' operator using the begin function from your first list

    //use interator obj to write new file from list


    if (finalfile.is_open())
    {
        while(finalfile << buff)
        {
        //use list iterator obj
        buff = *listIt;     //dereference overloaded op. obj
        finalfile << buff << endl;
        //listIt++;
        ++listIt;       //increment overloaded op.
        }
    }
    else
        cout << "file: 'lab3.out' could not be written to. " << endl;

    updatefile.close();
    initfile.close();

    cout << endl << endl << endl << "++++++++++++++++++++++++" << endl;
    cout << "files have been written..." << endl;

    //how to make dynamic objects so we can run destructor
    //so we don't have to manually call destroyList();
    //delete initList;  ?
    //delete listIt;    ?

return 0;
}
