#ifndef H_LINKEDLISTTYPE
#define H_LINKEDLISTTYPE
#include <iostream>
#include <cassert>
using namespace std;

/*
 * Cameron Heilman
 * COSC 2437-301
 * Lab3
 */



/*
 * derive from orderedlinkedlist fromhere
 * template <Type>
 */



//define link list node
template<class Type>
struct Node
{
    Type info;
    Node<Type> *link;
};


//CLASS LINKEDLISTTYPE
template <class Type>
class linkedListType
{
public:

    //overload = operator for the copy list function
    const linkedListType<Type>& operator= (const linkedListType<Type>&);

    //initialize list with first & last pointers == NULL, and counter = 0;
    //call destroyList() func inline function
    void startList()
    {   destroyList();   }

    //check if list is empty
    bool isEmpty() const
    {   return (first = NULL);   }

    //destroy list
    void destroyList();

    //get length of list
    //return count inline func
    int length() const
    {   return count;   }

    //print info
    void print() const;

    //return last element in list
    Type getLast() const;

    //return first element in list
    Type getFirst() const;

    //copy list function
    //void copyList(linkedListType<Type>& list2);

    //linkedlistIterator return iterator at beginning
    linkedListIterator<Type> begin();

    //and return iterator at end of list
    linkedListIterator<Type> end();

    //default constructor
    linkedListType();

    //copy constructor
    linkedListType(const linkedListType<Type>& otherlist);

    //destructor destroys list
    ~linkedListType();

    //virtual instert function defined in orderedList class
    //original code calls for virtual insertFirst() & insertLast() which only call insert function.
    //can I just make orderedlist::insert() virtual?
    //virtual void insert(const Type& newItem) const = 0;

    virtual void insertLast(const Type& newItem) = 0;

    virtual void insertFirst(const Type& newItem) = 0;

    //virtual search func defined in orderedList class
    virtual bool search(Type& searchItem) const = 0;

    //virtual delete note defined in orderedList class
    virtual void deleteNode(Type& deleteItem) = 0;


protected:

    //count elements in list
    int count;
    //set first & last pointers
    Node<Type> *first, *last;

private:
    //copies list
    //use this pointer
    void copyList(const linkedListType<Type>& otherlist);
};



//CLASS LINKEDLISTITERATOR
template <class Type>
class linkedListIterator
{

private:
    //create current pointer for traversal
    Node<Type> *current;

public:

    //default constructor sets current to null
    linkedListIterator()
    {   current = NULL; }

    //constructor with passing param
    linkedListIterator(Node<Type> *ptr)
    {   current = ptr; }

    //overloaded dereference operator comes in handy.
    Type operator*()
    {   return current->info;   }

    //overloaded. iterator advances to next node.
    linkedListIterator<Type> operator++()
    {   current = current->link; return *this;  }

    //true IF iterator == the iterator specified by "right"
    bool operator ==(const linkedListIterator<Type>& right) const
    {   return (current == right.current);  }

    //true IF iterator != the iterator specified by "right"
    bool operator !=(const linkedListIterator<Type>& right) const
    {   return (current != right.current);  }

};





/*
 *
//LIST TYPE IMPLEMENTATION
//--//
*
*/
//def. constructor setting def. values
template <class Type>
linkedListType<Type>::linkedListType()
{
    int count = 0;
    first = NULL;
    last = NULL;
}

template <class Type>
linkedListType<Type>::linkedListType(const linkedListType<Type>& otherlist)
{
    first = NULL;
    copyList(otherlist);
}

template <class Type>
void linkedListType<Type>::copyList(linkedListType<Type>& list2)
{
    Node<Type> *newNode;
    Node<Type> *current;

    //if list isn't empty. delete empty it
    if (first != NULL)
        destroyList();

    if(list2.first == NULL) //if other list is empty. just set everything to null <nothing to copy>
    {
        first = NULL;
        last = NULL;
        count = 0;
    }
    else
    {
        //point current to list2
        current = list2.first;
        count = list2.count;

        first = new Node<Type>; //new node
        first->info = current->info;
        first->link = NULL;

        last = first;
        current = current->link;

        while(current != NULL)
        {
            newNode = new Node<Type>;
            newNode->info = current->info;
            newNode->link = NULL;       //set newNode to last node.
            last->link = newNode;       //set newNode to follow behind "last" node
            last = newNode;             //set last to newNode
        }
    }
}

template <class Type>
void linkedListType<Type>::destroyList()
{
    Node<Type> *temp;

    while( first != NULL)
    {
        temp = first;
        first= first->link;     //move first to the next node
        delete temp;    //deallocate memory previous used by "first"
    }

    last = NULL;    //sets last to null as well
    count = 0;
}

template <class Type>
void linkedListType<Type>::print() const
{
    //Pre-condition:current must be instantiated to something other than NULL
    Node<Type> *current;
    while(current != NULL)
    {
        cout << current->info;
        current = current->link;
    }

}

//return type uses template.
template<class Type>
Type linkedListType<Type>::getFirst() const
{
    //is there a better way to do this?
    assert(first != NULL);
    return first->info;
}

template<class Type>
Type linkedListType<Type>::getLast() const
{
    assert(last != NULL);
    return last->info;
}


template <class Type>
linkedListIterator<Type> linkedListType<Type>::begin()
{
    linkedListIterator<Type> temp(first);
    return temp;
}

template <class Type>
linkedListIterator<Type> linkedListType<Type>::end()
{
    linkedListIterator<Type> temp(NULL);
    return temp;
}

 //destructor

template <class Type>
linkedListType<Type>::~linkedListType()
{
    destroyList();
}

/*
*
*
//ITERATOR IMPLEMENTATION
                            >>> EVERYTHING IS INLINE FUNCTIONS IN THE CLASS
//--//
*
*/



#endif
