#ifndef H_ORDEREDLINKEDLIST
#define H_ORDEREDLINKEDLIST

#include "LinkedListType.h"
#include <iostream>
using namespace std;


template <class Type>
class orderedLinkList: public linkedListType<Type>
{

public:

    using linkedListType<Type>::count;
    using linkedListType<Type>::first;
    using linkedListType<Type>::last;

    //search function -- virtual in linkedListType
    bool search(const Type& searchItem) const;

    //insert  function
    void insert(const Type& newItem);

    void insertFirst(const Type& newItem);

    void insertLast(const Type& newItem);

    //delete  function
    void deleteNode(const Type& deleteItem);
};



/*
 * IMPLEMENTATION
 */


template<class Type>
bool orderedLinkList<Type>::search(const Type& searchItem) const
{
    bool found = false;
    Node<Type> *current;

    current = first;    //start at first

    while (current != NULL && !found)
    {
        if (current->info >= searchItem)
            found = true;
        else
            current = current->link;
    }

    if (found)
        found = (current->info == searchItem);  //is it equal?


return found;
}

template<class Type>
void orderedLinkList<Type>::insert(const Type& newItem)
{
    Node<Type> *current,        //traversal
                *trailcurrent,  //pointer just before current
                *newNode;

    bool found;

    newNode = new Node<Type>;   //create new node
    newNode->info = newItem;    //store passed item into the node
    newNode->link = NULL;

    //1//
    if (first == NULL)  //if there is no list. start it.
    {
        first = newNode;
        last = newNode;
        count++;
    }
    else
    {
        current = first;
        found = false;

        //search list
        while (current != NULL && !found)
        {
            if(current->info >= newItem)
                found = true;
            else
            {
                trailcurrent = current; //use trailcurrent to keep track of previous node while moving current forward a node
                current = current->link;
            }
        }
        //2//
        if (current == first)
        {
            newNode->link = first;  //sets node right before first
            first = newNode;
            count++;
        }
        //3//
        else
        {
            trailcurrent->link = newNode;   //points to newNode step1
            newNode->link = current;        //newnode points to current. ultimately sliding in between trailCurrent && current in the ordered list.

            if (current == NULL)
                last = newNode;

            count++;
        }
    }
}

template<class Type>
void orderedLinkList<Type>::insertFirst(const Type& newItem)
{
    insert(newItem);
}

template<class Type>
void orderedLinkList<Type>::insertLast(const Type& newItem)
{
    insert(newItem);
}

template<class Type>
void orderedLinkList<Type>::deleteNode(const Type& deleteItem)
{
    Node<Type> *current, *trailcurrent;
    bool found;

    //1//
    if (first == NULL)
        cout << "list empty. nothing to delete." << endl;
    else
    {
        current = first;
        found = false;

        while (current != NULL && !found)   //search loop
        {
            if(current->info >= deleteItem)
                found = true;
            else
            {
                trailcurrent = current; //start trailcurrent-current node traversal
                current = current->link;
            }
        }

        if (current == NULL)
            cout << "the item: " << deleteItem << " could not be found in list" << endl << endl;

        //2//
        else
            if (current->info == deleteItem)    //if equal
            {
                if(first == current)
                {
                    first = first->link;    //move first 2 next node which deallocates previous node from memory

                    if (first == NULL)  //if there is no next node, set first and last to NULL.
                        last = NULL;

                    delete current;
                }
                else
                {
                    trailcurrent->link = current->link; //possible patch up link in list

                    if (current == last)
                        last = trailcurrent;

                    delete current;
                }
                count--;
            }
            else
                cout << "the item to be deleted is not in the list" << endl;
    }
}

#endif
