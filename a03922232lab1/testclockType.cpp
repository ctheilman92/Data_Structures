#include<iostream>
#include "clockType.h"
using namespace std;

//Name: Cameron Heilman
//Course: COSC 2437
//Lab #: 1
//Assigned: Sep 1, 2015
//Due: Sep 7, 2015, 11:59pm
//Purpose: To implement a simple mainline or driver program to
// test the clockType class found in Malik, chapter 1.
//
//Input: keyboard input for integer hours, minutes and seconds
//
//Output: screen output
//
//Program Outline/Design:
// -Create two clockType objects: cellPhoneClock and
// myComputerClock
// -Prompt the user to enter the times for both clocks.
// -Display both times with appropriate documentation.
// -If the clocks do not have equal times, set myComputerClock
// to the cellPhoneClock time.
// -Set myComputerClock so that it is 1 hour, 5 minutes,
// 23 seconds faster than the cellPhoneClock.
// -Display both times again, with appropriate documentation.
int main()
{


    //not sure if you can pass functions as another function's parameters.
   /*
    if(cellPHoneClock.equalTime(myComputerClock) == false)
        myComputerClock.setTime(cellPhoneClock.getHr(), cellphoneclock.getMin(), cellphoneclock.getsec());
    */




    int hours, minutes, seconds;    //for user input variables
    int getHr, getMin, getSec;      //for set & get function parameter passes
    //create the two objects
    clockType cellPhoneClock;
    clockType myComputerClock;

    cout << "this is a test driver for the implentation and header c++ files" << endl;


    //CELPHONE PROMPT FOR INPUT
    cout << "CELLPHONE CLOCK" << endl << "************************" << endl << endl;
    cout << "please enter the time for your cell phone. " << endl << "first hours: ";
    cin >> hours;
    cout << endl << "next minutes: ";
    cin >> minutes;
    cout << endl << "finally seconds: ";
    cin >> seconds;
    //run settime function for cellphone obj
    cellPhoneClock.setTime(hours, minutes, seconds);

    //COMPUTER PROMPT FOR INPUT
    cout << endl << endl << endl;
    cout << "COMPUTER CLOCK" << endl << "************************" << endl << endl;
    cout << "please enter the time for your computer. " << endl << "first hours: ";
    cin >> hours;
    cout << endl << "next minutes: ";
    cin >> minutes;
    cout << endl << "finally seconds: ";
    cin >> seconds;
    //run gettime function for computer obj. then settime.?
    myComputerClock.setTime(hours, minutes, seconds);


    //local hours,minutes, seconds vars set to match myComputerClock private vars

    cout << endl << endl << "great. here are the times you set." << endl
         << endl;
    //get and display cellphone clock.
    cellPhoneClock.getTime(getHr, getMin, getSec);         //check to see if this is correct
    cout << "CELL TIME: ";
    cellPhoneClock.printTime();


    //get and display computer clock.
    cout << endl << endl << "++++++++++++++++++++++++" << endl << endl;
    myComputerClock.getTime(getHr, getMin, getSec);        //check to see if this is correct
    cout << "COMPUTER TIME: ";
    myComputerClock.printTime();



    //ask about operator overloading to set private variables equal to each other


    //start equaltime checker(EITHER USE A COPY OPERATOR OR USE SET GET AGAIN!)
    //if mycomputer clock doesn't equal cellphone clock set my computer equal to cellphone
    cout << endl << "***********" << endl << "now checking if the times match..." << endl << endl;
    if(myComputerClock.equalTime(cellPhoneClock) == false)
    	{
	cout << "EQUAL TIME HAS FAILED. RESETTING CLOCKS" << endl << endl;
       	//set local vars to match private cellphone vars, then set private computer vars to match
       	cellPhoneClock.getTime(getHr, getMin, getSec);
       	myComputerClock.setTime(getHr, getMin, getSec);
	

       cout << endl << "CELL TIME: ";
	 cellPhoneClock.printTime();
       cout << endl << "COMPUTER TIME: ";
	 myComputerClock.printTime();
       cout << endl;
    	}

    else
	cout << "clocks are equal already. continuing. " << endl << "**********************" << endl;

    //set computer clock 1hr, 5min, 23sec faster than cellphone clock
    //run incrementhours() once.
    //run for loop for incrementmin 5x
    //run for loop for incrementsec 23x
    cout << endl << "**************" << endl << "great now we will set the computer clock 1 hour, 5 minutes and 23 seonds faster" << endl;
    myComputerClock.incrementHours();

    for(int i = 0; i < 5; i++)
    {
        myComputerClock.incrementMinutes();
    }

    for(int j = 0; j < 23; j++)
    {
        myComputerClock.incrementSeconds();
    }

    //display times
    cout << endl << endl << "CELL TIME: "; 
	cellPhoneClock.printTime();
    cout << endl << "COMPUTER TIME: ";
	myComputerClock.printTime();

	cout << endl << endl;






return 0;	
	
}
