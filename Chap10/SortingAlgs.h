#include <iostream>
#include <string>

using namespace std;


/*

NOTES FOR DIFFERENT SORTING METHODS
REFER TO LAB 8
*/


//insertion sort
/*
			//array based//
	1) find smallest # in unsorted list
	2) swap it with whatever # is in list[0]
	3) unsorted list range is [1] to [n]
*/



//find smallest number position
template <class type>
int arrayListType<type>::minLocation(int first, int last) {

	int minIndex = first;

	for (int loc = first + 1; loc <= last; loc++) {
		if (list[loc] < list[minIndex])
			minIndex = loc;

	}
	return minIndex;	
}

//then swap function
//second will substitute minLocation
void arrayListType<type>::swap(int first, int second) {
	type temp;

	temp = list[first];
	list[first] = list[second];
	list[second] = temp;
}

template <class type>
void arrayListType<type>::selectionSort() {

	int minIndex;

	for (int loc=0; loc<length-1; loc++) {
		minIndex = minLocation(loc, length-1);
		swap(loc, minIndex);
	}
}

/*

ANALYSIS:
	FOR LIST SIZE = k;

	->minlocation(int, int) makes (k-1) comparisons and decrements k-1 each time.
			::(k-1) + (k-2):: etc.

	->minLocation(int, int) is executed (n-1) times by selectionSort()


	THEREFORE:::>>>
	 O(n^2) = (1/2)n^2 -(1/2)n;
	 for n=1000;

	 1/2(1000^2) - 1/2(1000) = 499500 comparisons would be made
*/


//INSERTION SORT

template <class type>
void arrayListType<type>::insertionSort() {

	int firstToOrder;
	int temp;
	int loc;	//keep track of position of the item being sorted
	
	for (firstToOrder = 1; firstToOrder < length; firstToOrder++)
	{
		if(list[firstToOrder] < list[firstToOrder -1]) {
			temp = list[firstToOrder];	//copy sortItem to temp
			loc = firstToOrder;

			do {

				//move list[loc-1] one slot down
				list[loc-1] = list[loc];
				//decrement loc by 1 to consider the next element to sort
				loc--;
			}
			while (loc > 0 && list[loc-1] > temp)
		}//if

		//copy temp into new location
		list[loc] = temp;
	}
}


/*
	ANALYSIS OF INSERTION SORT
	+++++++++++++++++++++++++++

	list length = n;

		best case(list already sorted)
			comparisons = (n-1);
			assignments = 0;

		worst case(reverse sorted)
			comparisons = (1/2)(n^2 -n);
			assignments = (1/2)(n^2 - 3n) -2;

		AVERAGE CASE
			comparisons = (1/4)n^2 + O(n) = O(n^2);
			assignments = (1/4)n^2 + o(n) = O(n^2);


		AKA insertion sort has the same amount of comparisons as selection 
		()
*/























/*
	SHELL SORT
		multiple sublists

		sublist of distance partitions

		increment sequence: best formula = 3 *(k-1)+1;
*/

template <class type>
void arrayListType<type>::shellSort() {

	//iteration counting increment
	int k;

	for (k = 1; k < (length -1) / 9); k = 3*k+1);
	
	do {

			//this traverses each sublist, the function call is the literal sort function
		for (int begin = 0; begin < k; begin++)
			intervalInsertionSort(begin, k);

		k = k/3;
	}
	while (k > 0);
}

//for the intervalInsertionSort() 
//
template <class type>
void arrayListType<type>::intervalInsertionSort(begin, k) {

	//each sublist starts at *begin*

	//MODIFIED INSERTION SORT where sublist begins at *begin* and increments between elements given by k
}

















/*
	QUICKSORT
	+++++++++++++++++++++++++++
	
	divide & conquer (split into 2 sublists)
	using recursion, the partitioning does all the work actually

	pivot: element close to or in the middle of the list (use to partition)
	smallIndex: last element < pivot (initialized to first element in list);
	index: the position of the next element to be sorted
	


	****partition algorithm*****
	1a. determine pivot
	1b. swap pivot with list[0];
	1c. smallIndex pointer initailized to first element in list
	
		for remaining elements in list
		(use for-loop starting at 2nd element in list)
		if current element is smaller than pivot
		2a. increment smallIndex;
		2b. swap current element with array element pointed to by smallIndex;

			3. swap the first element (pivot) with the element pointed by smallIndex;
*/



//QUICKSORT PARTITION FUNCTION
			//reminder: swap() defined above
template <class type>
int arrayListType<type>::partition(int first, int last) {

	type pivot;
	int index, smallIndex;

	//STEP 1 (move pivot to [0] & initialize smallIndex)
	swap(first, (first+last)/2);
	pivot = list[first];
	smallIndex = first;

	//STEP 2 EXECUTION
	for (index = first+1; index <= last; index++) {
		
		if (list[index] < pivot) {
			smallIndex++;
			swap(smallIndex, index);
		}
	}

	//STEP 3
	//swap pivot to be back in the middle
	swap(first, smallIndex);
	return smallIndex;
}


//RECURSIVE QUICKSORT FUCKTION
//pivotLocation calls partition()

template <class type>
void arrayListType<type>::recQuickSort(int first, int last) {

	int pivotLocation;

	if (first < last) {
		pivotLocation = partition(first, last);
		recQuickSort(first, pivotLocation);			//QUICKSORT LOWERBOUND
		recQuickSort(pivotLocation+1, last);		//QUICKSORT UPPERBOUND
	}
}

//DRIVING QUICKSORT FUNCTION
template <class type>
void arrayListType<type>::quickSort() {

	recQuickSort(0, length-1);
}



















/*
	MERGE SORT
	+++++++++++++++++++++++++++

	****always O(n(log_2 n))****

	.partitions list into 2 equal sizes(or almost-equal)
	.continuously partitions into smaller sizes until comparing 2 lists of size 1.
	.after sorting(they are merged with upper level lists).

	handled recursively as well.

	if (listsize > 1)
		1.	divide list into 2 sublists.
		2. mergeSort the first sublist
		3. mergeSort the second sublist
		4. merge the first & second sublists
*/


//THIS FUNCTION WILL BE FOR LINKEDLIST
template <class type>
nodeType<type>* unorderedLinkedList<type>::mergeList(nodeType<type>* first1, nodeType<type>* first2) {

	nodeType<type> *lastSmall;		//pointer to the last node of merged list
	nodeType<type> *newHead;		//pointer to the merged list


	if (first1 == NULL)				//first sublist = empty
		return first2;
	else if (first2 == NULL)
			return first1;

	else {

		if (firsrt1->info < first2->info)	//compare nodes
		{
			newHead = first1;
			first1 = first1->link;
			lastSmall = newHead;
		}
		else {
			newHead = first2;
			first2 = first2->link;
			lastSmall = newHead;
		}

		while (first1 != NULL && first2 != NULL) {

			if (first1->info < first2->info) {

				lastSmall->link = first1;
				lastSmall = lastSmall->link;
				first1 = first1->link;
			}
			else
			{
				lastSmall->link = first2;
				lastSmall = lastSmall->link;
				first2 = first2->link;

			}

			if (first1 == NULL)		//first sublist is exhausted first
				lastSmall->link = first2;
			else 					//2nd sublist is exhausted first
				lastSmall->link = first1;

			return newHead;
		}
	}
}






/*
	MERGE SORT

	low = root node;
	high = last item in list



*/

template <class type>
void arrayListType<type>::heapify(int low, int high) {

	//where low is root node of subtree, high is the index of last item in the list

	int largeIndex;		//index of left child
	type temp = list[low];	//copy root node 


	while (largeIndex <= high) {

		if (largeIndex < high)
			if (list[largeIndex] < list[largeIndex + 1])		
				largeIndex = largeIndex + 1;					//STEP 1 COMPARE CHILDREN

		if (temp > list[largeIndex])						//STEP 2 (if subtree is already in heap)
			break;
		else {
			list[low] = list[largeIndex];		//move larger child to root
			low=largeIndex;
			largeIndex = 2 * low + 1
		}
	}//end while
	list[low] = temp;	//insert temp into the tree(list)
}

template <class type>
void arrayListType<type>::buildHeap() {

	for (int i = length/2-1; i >=0; i--) {
		heapify(index, length-1);
	}
}

//FINALLY
template <class type>
void arrayListType<type>::heapSort() {

	type temp;

	buildHeap();	//first round to build the heap. (initial points are middle & last)

					//2nd build counts down with the remainder of the elements
	for (int lastOutofOrder = length -1 lastOutofOrder >= 0; lastOutofOrder--) {
		temp = list[lastOutofOrder];
		list[lastOutofOrder] = list[0];
		list[0] = temp;
		heapify(0, lastOutofOrder-1);
	}
}